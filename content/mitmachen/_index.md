---
title: "Mitglied werden!"
subTitle: >
        Los geht's: Kein formaler Mitgliedsantrag kann dich jetzt noch aufhalten!
---
Du kannst den ausgefüllten Antrag gern vorbei bringen oder per Mail an [vorstand@z-labor](mailto:vorstand@z-labor.space) senden. Wir melden uns dann mit weiteren Infos. Schau dir im Folgenden auch unsere Satzung an:

<a href="/documents/mitgliedsantrag_z-labor.pdf" class="btn btn-primary mb-3" style="min-width: 200px; margin-left: 8px; margin-right: 8px;">
        Mitgliedsantrag
</a>
<a href="/documents/vereinssatzung_z-labor.pdf" class="btn btn-primary mb-3" style="min-width: 200px; margin-left: 8px; margin-right: 8px;">
        Satzung
</a>
<br><br>

## Spenden

Du kannst uns auch gern mit einer Geldspende unterstützen: 

```
        z-Labor e.V. 
        IBAN: DE96830654080004269713
        BIC: GENODEF1SLR
        Deutsche Skatbank
```
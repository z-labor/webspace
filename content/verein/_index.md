---
title: "Das z-Labor"
date: 2022-01-08T10:41:03+06:00
subTitle: >
        Wir sind ein Sammelbecken für Menschen, die sich sowohl kreativ als auch kritisch mit (digitalen) Technologien auseinandersetzen.
---

## Offener Hackspace
Die Bandbreite unserer Themen und Projekte reicht von Elektronik und IT-Sicherheit über Amateurfunk und Netzwerktechnik bis hin zu  (Kunst-)Handwerk, Siebdruck und Fotografie. Neben 3D-Druckern, Lötstationen, Mikrocontrollern und Rechentechnik gehört zu unserem Space eine Holz- und Metallwerkstatt, eine Musik-Ecke, Retro-Games sowie ein Nähbereich. 

Wir lieben offene und freie Software, sind Fans von Linux-Systemen und veranstalten regelmäßig Vorträge, Spiele- und Filmabende sowie Löt- und Bastelworkshops für Kinder und alle anderen.

## Schön ist, was neugierig macht
Im z-Labor e.V. erwarten dich Menschen, die technisch interessierten Spaß am Tüfteln haben und neugierig sind, wie Geräte oder Systeme funktionieren und sich nicht scheuen, diese auseinander zu nehmen und kreativ für die eigenen Zwecke umzufunktionieren. 

## All Creatures Welcome
Wir wollen einen kreativen und diskriminierungsfreien Raum gestalten, in dem wir gesellschaftlich alle einen Nutzen finden können. Wir tolerieren keine Art der Belästigung oder Diskriminierung von Gästen oder Mitgliedern aufgrund ihrer äußeren Erscheinung, Geschlechtsidentität, sexuellen Orientierung, Behinderungen, Hautfarbe oder ethnischen Zugehörigkeit. 

<!--
## Kooperationen
- Freifunk Zwickau
- Computertruhe e.V.
- Mekomobil
- Chaoszone
- Kulturweberei



## Dies ist unsere Geschichte
Betreutes Basteln auf aller niedrigstem Niveau - unter diesem Credo haben wir uns 2015 zusammen gefunden, um auch in Zwickau einen Hackspace zu starten.


sliderImage:
  - image: "images/stor/story-01.jpg"
  - image: "images/stor/story-02.jpg"
  - image: "images/stor/story-03.jpg"
---

-->
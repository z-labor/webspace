---
title: 'Erste Eindrücke'
date: 2015-01-18T21:54:00+06:00
featureImage: /blog/erste-eindruecke/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "2015"]
---

Nach und nach füllt sich unser z-Labor mit diversen Werkzeugen und Geräten. Hier gibts nun mal einen ersten Eindruck vom Raum und der Ausstattung. Mit ausreichend Leuchten haben wir den an sich recht rustikalen Raum erhellt und schon mal mit einer kleinen Musikanlage für die richtige Stimmung gesorgt. Auf zwei Seiten gibt es ausreichend Arbeitsplätze zum Löten, Schrauben, Bohren, Hacken, Machen und so weiter.

![](/blog/erste-eindruecke/dsc_0295.webp)

Neben diversen Lötkolben/-stationen gibts auch schon einiges an elektronischen Komponenten, Evaluation-Boards, Messgeräten und Literatur. Hier sind wir schon ziemlich gut ausgestattet, so dass den ersten Projekten nichts im Weg steht. Es kann bei Bedarf von mehreren Leuten gleichzeitig der Lötkolben geschwungen werden!

![](/blog/erste-eindruecke/dsc_0311.webp)
![](/blog/erste-eindruecke/dsc_0297.webp)
![](/blog/erste-eindruecke/dsc_0310.webp)

Außerdem haben wir noch eine Ecke fürs Grobe mit Bohrmaschine, Flex, Elektroden-Schweißgerät etc. eingerichtet. Wer sich an Metall, Holz oder Kunststoff irgendwie mechanisch vergehen will ist hier genau richtig. Auch diverse Kleinwerkzeuge wie Schraubenzieher, Maulschlüssel und Panzertape können exzessiv malträtiert werden.

![](/blog/erste-eindruecke/dsc_0298.webp)

Ach so, falls die Bastelei mal wieder länger geht haben wir auch noch ein gemütliches, ausziehbares Sofa zu bieten….! (Anmerkung der Redaktion: irgendwie wird das Sofa mittlerweile noch recht häufig zu Abstellzwecken missbraucht)

![](/blog/erste-eindruecke/dsc_0300.webp)

Demnächst wird noch ein Platz zur Verarbeitung von Faserverbundwerkstoffen, eine kleine Chemieecke und ein Whiteboard installiert. Updates kommen über Twitter und diesen Blog oder vor Ort, wenn ihr uns besucht!
---
title: 'Wir machen Chaos!'
date: 2019-10-12T00:24:00+06:00
featureImage: /blog/wir-machen-chaos/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "2019", "ccc", "chaostreff"]
---

Im z-Labor wird es ab sofort jeden Mittwoch ab 19:30 Uhr neben dem offenen Abend auch einen [Chaostreff](https://www.ccc.de/de/club/chaostreffs) geben. Alle Lebensformen, die sich dem [Chaos Computer Club](https://www.ccc.de/de/regional) verbunden fühlen, sind herzlich eingeladen mit uns ins Gespräch zu kommen, Ideen einzubringen und Projekte zu realisieren.

Als größter europäischer Hackerverein ist der CCC ein Sammelbecken für Menschen, denen sowohl der kreative als auch der kritische Umgang mit (digitalen) Technologien sowie eine freie Netzkultur am Herzen liegen. Da wir uns mit den Idealen des Clubs identifizieren können, möchten wir mit dem Chaostreff auch anderen in Zwickau und Umgebung die Möglichkeit zum Austausch bieten.

Weitere Informationen zum Wirken und der Struktur des Chaos Computer Clubs unter [ccc.de/de/club](https://www.ccc.de/de/club) und [ccc.de/de/regional](https://www.ccc.de/de/regional).

Seid großartig zueinander!

![](/blog/wir-machen-chaos/030dfef6a5f6b693.jpeg)
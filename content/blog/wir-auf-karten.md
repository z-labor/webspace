---
title: 'Wir auf Karten'
date: 2018-02-28T23:22:00+06:00
featureImage: /blog/wir-auf-karten/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "2018", "openstreetmap", "opensource"]
---

Wir sind mittlerweile auch auf einer Anzahl an Karten zu finden. Wer [OpenStreetMap](https://www.openstreetmap.org/node/5322481262#map=17/50.72237/12.48265&layers=HD) verwendet, kann dort einen Knoten in der richtigen Location mit unserem Namen finden.

![](/blog/wir-auf-karten/osm_zlabor.webp)

Ebenso findet ihr uns im Verzeichnis der Hackerspaces unter [hackerspaces.org](https://wiki.hackerspaces.org/Z-Labor). Und da wir immer durstig sind haben wir uns gleich mal noch im Verzeichnis der Hackerbrause eingetragen, ihr findet uns nun auch bei [MateMonkey](https://matemonkey.com/map/dealer/z-labor?@=16,50.725304,12.485694) ! Das heißt natürlich auch, dass wir wieder Mate vorrätig haben ;-). Kommt vorbei!
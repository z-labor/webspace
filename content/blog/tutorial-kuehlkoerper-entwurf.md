---
title: 'Tutorial: Kühlkörper Entwurf'
date: 2016-06-15T21:27:00+06:00
featureImage: /blog/tutorial-kuehlkoerper-entwurf/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "2016"]
---

Schönen guten Tag, mein Name ist Markus und ich bin ebenso wie Christopher Mitglied des Z-Labor und studiere an der Westsächsische Hochschule Zwickau Elektrotechnik. Derzeit beschäftige ich mich in einen Projekt um die Kühlung von Leistungselektronik und möchte daraus einen Artikel für unseren Blog erstellen.

**Widerstand**

Grundlegend funktioniert die thermische Betrachtung similar zum Ohm’schen Gesetz. Dabei ergibt sich die Temperaturerhöhung aus der umgesetzten Leistung und dem entgegengesetzten Wärmewiderstand:

![](blog/tutorial-kuehlkoerper-entwurf/1.webp)

Die einzelnen Wärmeübergangswiderstände sind in Reihe geschaltet und der Gesamtwiderstand ergibt sich aus der Summe der einzelnen thermischen Widerstände:

![](blog/tutorial-kuehlkoerper-entwurf/2.webp)

Jetzt wissen wir das man einzelne Widerstände addieren können, aber wo bekommt man die her und was bedeuten die im allgemeinen?

Das ist eigentlich recht einfach im Datenblatt des Halbleiters sind meistens 2 Werte angegeben. Das ist der Wärmewiderstand für das Silizium bis zur Umgebungsluft ohne Kühlkörper(Rja) oder der Wärmewiderstand vom Silizium zum Gehäuse(Rjc). Die Werte vom Gehäuse zum Kühlkörper(Rcs) kommt auf den verwendeten Wärmeübergang an. Wird Wärmeleitpaste verwendet kann meistens 1 °C/W angenommen werden. Der letzte Wert(Rsa), ist der Widerstand vom Kühlkörper zur Umgebungstemperatur und wird beim kaufen des Kühlkörpers angegeben.

![](blog/tutorial-kuehlkoerper-entwurf/widerstand.webp)

So, jetzt haben wir zwar diese Formeln, aber wie warm wird es jetzt nun eigentlich unseren kleinen tapferen Stück Silizium?

Die Temperatur im Inneren des IC´s ergibt sich aus der Umgebungstemperatur(Ta meist 25°C angenommen) und der Temperaturerhöhung(ΔT):

![](blog/tutorial-kuehlkoerper-entwurf/3.webp)

Wenn wir jetzt alles in eine Formel einsetzen bekommen wir die folgende Formel:

![](4.webp)

Mit dieser Formel ist es möglich die erreichte Temperatur des Silizium zu berechnen und mit den maximal Ratings aus dem Datenblatt zu vergleichen. Ich würde aber selbst nie versuchen die maximal Temperatur zu erreichen, sondern mindestens 30% Reserve zulassen. Da sonnst die Lebensdauer des Bauelements darunter leidet.

Ich hoffe das der Beitrag euch etwas Hilft und eure Bauteile keine heißen Köpfe mehr bekommen.

Viel Spaß beim Basteln
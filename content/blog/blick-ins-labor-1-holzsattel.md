---
title: 'Projekt Holzsattel'
date: 2016-01-05T18:47:00+06:00
featureImage: /blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/thumbnail.webp
tags: ["hackerspace", "makerspace", "2016"]
---

An dieser Stelle starten wir die Serie Blick ins Labor, die Projekte aus unserem Hacker- und Makerspace vorstellt. Den Anfang macht das Projekt Holzsattel von [Christopher](https://twitter.com/ch_taudt):

„…according to family legend, the company began when founder John Boultbee Brooks, a horse saddle manufacturer, tried to use a bicycle after his horse died but found the wooden seat very uncomfortable. As a result, he vowed to set about solving this problem and Brooks was born.“ Mit diesen Worten zittiert die [Wikipedia](https://en.wikipedia.org/wiki/Brooks_England) beziehungsweise eine [Firmensprecherin](http://www.feeldesain.com/8-questions-with-michela-raoss-%E2%80%A2-communication-manager-at-brooks-england.html) von Brooks die Entstehungsgeschichte der legendären Leder-Fahrradsattelmarke. Wenn das mal nicht die perfekte Herausforderung für einen Hack ist, dachte ich mir! Ein Sattel aus Holz muss doch bequem und fahrbar sein können. Nach einem ersten gescheiterten Versuch bin ich also wieder im Labor verschwunden und habe den Sattel von unten neu gedacht:

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20140619-dsc_0929.webp)

Das heißt ich habe zuerst überlegt wie ich Komfort sprich Federung in den Sattel bekomme. Beim ersten Versuch hatte ich als Gestell unter dem Sattel einfach die Metallstangen eines handelsüblichen Sattels genutzt. Die sind sehr starr und bieten gar keine Federwirkung. Daraus entstand die Idee eine Blattfeder nachzuahmen wie sie früher in vielen Kutschen, Autos und anderen Fahrzeugen eingesetzt wurden. Auch das habe ich erstmal getestet indem ich ein Stück Skateboard passend zurecht gedengelt habe:

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20151104-img_20151104_073254.webp)

Als Holz habe ich auch sehr schnell sogenanntes Starkfurnier ausgewählt, was ich in mehreren Lagen übereinander zusammenkleben wollte. Damit kann man sehr genau die nötige Dicke der Konstruktion und damit die Federwirkung einstellen. Außerdem kann man solch vergleichsweise dünnen Holzstücken (ca. 2 mm) einfacher als Vollholz in Form biegen.

Der eigentliche Plan war es die Form des Untergestells dem eines klassischen Sattels nachzubauen. Auch hierzu habe ich Skizzen und eine erste Form angefertigt.

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20141213-img_20141213_221159.webp)

Im Laufe des Prozesses hat sich aber schnell gezeigt, dass die Form zum Kleben mit vielen Schraubzwingen ungeeignet und das Vorbiegen des Holzes zu schwierig ist. Ich habe die Form daraufhin stark vereinfacht, sodass sie eigentlich nur noch ein recht großer Radius ist. Zur Vorbereitung konnte ich einzelne, nasse Lagen Furnierholz (hier: Okume Gabune…war billig auf Ebay) über einer heißen Stange vorbiegen und nach dem Trocknen miteinander verleimen. Momentan klebe ich alles mit normalem Weißleim, allerdings experimentiere ich auch mit natürlichen Alternativen wie [Kasein-Leim](http://www.velomobilforum.de/wiki/doku.php?id=holzbau:kaseinleim). Ich habe hier insgesamt 5 Schichten verleimt und anschließend in die richtige Form geschnitten.

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20151107-img_20151107_142837.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20151107-img_20151107_144216.webp)

Den Sattel selbst habe ich in ähnlicher Weise aufgebaut. Auch hier habe ich das gleiche Furnier zuerst zugeschnitten und nachher verleimt. Als Vorlage habe ich die Form eines richtigen Sattels digitalisiert. Diese Grundform habe ich dann für die einzelnen Lagen immer verkleinert und angepasst um schon die grobe Kontur mit den einzelnen Schichten zu erhalten. Um etwas Gewicht zu sparen, wurden die ersten fünf Lagen in der Mitte ausgeschnitten. Der fertig geklebte Sattel musste nur noch ein wenig in Form geschliffen und an einzelnen Stellen gespachtelt werden um eine glatte Oberfläche zu erhalten. Als Spachtel bietet sich ein Gemisch aus Schellack (in Alkohol gelöst) und Schleifstaub an. Das Gemisch lässt sich ganz gut spachteln und härtet innerhalb von 1-3 Tagen durch.

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20141221-img_20141221_145313.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20150113-img_20150113_211830.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20150704-dsc_0307.webp)

Zum Test bin ich den fertigen Sattel jeweils mit dem Skateboardunterteil und mit der selbstgebauten Blattfeder Probegefahren. Und, was soll ich sagen? Er fährt sich SUPER! Völlig bequem und nicht härter als ein normaler Sattel, wirklich! Durch die Blattfeder und den Schichtaufbau hat das System eine ganz vernünftige Flexibilität, die auch mal ein Schlagloch abfedert. Die erste Runde war gleich zwei, drei Kilometer lang und nach mir haben sich auch schon Zeugen getraut eine kleine Runde zu drehen!

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20151202-img_20151202_202752.webp)

Leider muss ich auch zugeben, dass die selbstgebaute Blattfeder nicht ganz so stabil war wie ich gehofft hatte. Auf einer der Probefahrten ist sie gebrochen und hat den Sattel beschädigt. Hier gibt es also noch etwas Luft für eine Version 0.3 ;-). Außerdem arbeite ich an einem Skript, um die einzelnen Lagen des Furniers demnächst mit dem Lasercutter zu schneiden. Für die nicht so harten Mädels oder Jungs habe ich auch schon einen Sattel aus Kork gebaut, der auch mit einer neuen Blattfeder getestet wird.

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20150704-dsc_0309.webp)

Wer sich zu einer Probefahrt traut oder einfach nur einen Blick auf den Sattel werfen möchte kann gern bei uns im z-Labor vorbeikommen!

Christopher ist Mitglied im z-Labor und bloggt ab un zu über Hacks- und Bastelein auf [inside-these-boxes](https://insidetheseboxes.wordpress.com/). Auf twitter könnt ihr ihn finden unter [@ch_taudt](https://twitter.com/ch_taudt):

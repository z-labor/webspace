---
title: 'Frühjahrsputz'
date: 2019-04-11T23:45:00+06:00
featureImage: /blog/fruehjahrsputz/thumbnail.webp
tags: ["hackerspace", "makerspace", "spaceapi", "2019"]
---

Bei uns verändert sich mal wieder einiges. So haben wir nicht nur unseren Space in den letzten Monaten auf Vordermann gebracht, sondern auch unserer Infrastruktur ein Update verschafft. Ihr habt sicher schon gemerkt, dass ihr uns im Web jetzt unter [www.z-labor.space](http://www.z-labor.space/) erreicht. Ebenso könnt ihr uns per Mail unter [info@z-labor.space](info@z-labor.space) erreichen. Außerdem haben wir im Hackint IRC Netzwerk den Kanal [#z-Labor](irc://irc.hackint.eu/#z-labor) angelegt über den ihr spontan mit uns chatten könnt. Eine weitere Neuigkeit ist unsere [SpaceAPI](https://spaceapi.fixme.ch/directory.json) Eintrag. [Hier](http://api.service.z-labor.space/spaceapi.json) findet ihr unser zugehöriges JSON-file welches euch nicht nur grundlegende Eigenschaften wie Name, Ort und Größe unseres Spaces zeigt, sondern auch ob wir offen sind oder ob das Licht angeschalten ist. Unter Android werden diese Informationen zum Beispiel durch die [My Hackspace App](https://play.google.com/store/apps/details?id=ch.fixme.status&hl=en_US) kombiniert, im Web bietet [Spacer](https://spacer.nooitaf.nl/) eine Übersichtskarte. Schlussendlich findet ihr auch im [Hackerspaces Wiki](https://wiki.hackerspaces.org/Z-Labor) einen aktualisierten Eintrag zum z-Labor mit Fotos.

Mit all den Neuigkeiten sind wir aber noch lang nicht fertig. Wir arbeiten derzeit an der Umgestaltung und Erweiterung unseres Werkstattbereichs. Dazu wird es dann demnächst einiges zu erzählen geben.
    
![](/blog/fruehjahrsputz/20190117-img_6013.webp)
![](/blog/fruehjahrsputz/20190117-img_6014.webp)
![](/blog/fruehjahrsputz/20190117-img_6015.webp)
![](/blog/fruehjahrsputz/20190117-img_6008.webp)
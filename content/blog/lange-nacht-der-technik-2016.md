---
title: 'Lange Nacht der Technik 2016'
date: 2016-03-05T18:55:00+06:00
featureImage: /blog/lange-nacht-der-technik-2016/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "2016", "veranstaltung"]
---

Mittlerweile eine feste Größe im alternativen Veranstaltungskalender der Stadt Zwickau ist die Lange Nacht der Technik. Jedes Jahr Ende April öffnet die [Westsächsische Hochschule](http://fh-zwickau.de/) zusammen mit dem [August-Horch-Museum](http://www.horch-museum.de/index.php) an einem Abend alle Türen, um mit etwas Show und einer Menge Spaß Aktuelles aus Wissenschaft und Technik zu zeigen. Bei Bratwurst, Live-Musik und Unterhaltung ist das Ganze nicht nur spannend sondern auch leicht bekömmlich. 

Wer danach auf den Geschmack gekommen ist und sich spielerisch mit Technik auseinandersetzen will ist natürlich bei uns perfekt aufgehoben… =)


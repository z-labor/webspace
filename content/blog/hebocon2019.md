---
title: 'Das z-Labor auf dem 36c3: Killer Bee for Future'
date: 2020-02-19T01:50:00+06:00
featureImage: /blog/hebocon2019/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "2020", "36c3", "congress", "ccc", "leipzig", "veranstaltung"]
---

Der [36. Chaos Communication Congress](https://events.ccc.de/congress/2019/wiki/index.php/Main_Page) (36c3) lockte 2019 zwischen Weihnachten und Neujahr wieder tausende Hacker und Haecksen sowie andere technisch und politisch interessierte Menschen in die Leipziger Messe. Auch das z-Labor beteiligte sich an der alljährlichen Hacking-Konferenz des [Chaos Computer Clubs](https://www.ccc.de/), um die Community mit Rat und Tat zu unterstützen und sich unter dem Motto „Resource Exhaustion“ intensiv mit Technologie und deren Folgen für Gesellschaft und Umwelt auseinanderzusetzen.

Schon eine Woche vor Beginn halfen wir beim Aufbau der Area der [ChaosZone](https://wiki.c3d2.de/ChaosZone), welche uns und weiteren Hackerspaces aus Brandenburg, Sachsen, Thüringen und Osteuropa Unterschlupf gewährte sowie Erfahrungsaustausch und Zusammenarbeit ermöglichte. Doch nicht nur als sogenannte Aufbau-Engel waren wir im Einsatz. Wir meldeten uns zum Beispiel freiwillig für Schichten als Saal-Engel, um den Zuschauer:innen vor Vorträgen die Plätze zuzuweisen.

Auch die Hebocon – ein fulminanter low-tech Roboter-Wettkampf – wurde so von Mitgliedern des z-Labors maßgeblich mitorganisiert und erwies sich für uns als Highlight des Jahres. Zum einen standen wir den Teilnehmenden bei der Fertigstellung ihrer Schrott-Roboter zur Seite. Zum anderen schickten wir unsere eigene Protagonistin, die sogenannte „Killer Bee“ ins Rennen. Außerdem behielt ein Zwickauer Laborant auf der Bühne die Kontrolle über den Spielverlauf und die Rangliste. Vor rund 5000 begeisterten Zuschauer:innen, die den Wettbewerb der Trash-Roboter vor Ort verfolgten, konnte sich unsere kleine summende Biene überraschenderweise bis ins Finale durchschlagen, um den im Vergleich gigantisch wirkenden „Typhoon“ aus dem Ring zu schieben. Ihr Siegeszug konnte nur von Schrubbi, dem Gewinner des Vorjahres, gestoppt werden. Am Ende war es ein langer und spannender Kampf, welchen unsere z-Labor Biene leider aufgrund fehlender Ausdauer verlor. Dafür brummte sich Killer Bee in viele Herzen und sorgte zeitweise sogar für einen regelrechten Fankult.

Wenn ihr Live nicht dabei sein konntet oder den Auftritt verpasst habt, könnt ihr euch hier die Aufzeichnung anschauen und ordentlich mit fiebern:



Weitere Empfehlungen für sehenswerte Talks haben wir im Folgenden für euch aufgelistet:

[Let’s play Infokrieg: Wie die radikale Rechte (ihre) Politik gamifiziert](https://media.ccc.de/v/36c3-10639-let_s_play_infokrieg)

[Hirne Hacken: Menschliche Faktoren der IT-Sicherheit](https://media.ccc.de/v/36c3-11175-hirne_hacken)

[Welches Betriebssystem hat der Bundestag und wie kann man es hacken?](https://media.ccc.de/v/36c3-106-welches-betriebssystem-hat-der-bundestag-und-wie-kann-man-es-hacken-#t=1016)

[Inside the Fake Like Factories: How thousands of Facebook, YouTube and Instagram pages benefited from purchased likes](https://media.ccc.de/v/36c3-10936-insidethefakelikefactories#t=842)

[BahnMining: Pünktlichkeit ist eine Zier](https://media.ccc.de/v/36c3-10652-bahnmining_-_punktlichkeit_ist_eine_zier)

[Das Mauern muss weg: Best of Informationsfreiheit](https://media.ccc.de/v/36c3-10496-das_mauern_muss_weg)

[Die Affäre Hannibal](https://media.ccc.de/v/36c3-11114-die_affare_hannibal)

Natürlich wird sich das z-Labor auch 2020 wieder auf der ChaosZone Assembly am Congress beteiligen und unsere Biene womöglich ein paar Wochen ins Bootcamp schicken oder ihr zumindest ein paar Verbündete zusammenlöten. Wir freuen uns schon riesig!
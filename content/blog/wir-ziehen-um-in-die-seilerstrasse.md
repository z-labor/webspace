---
title: 'Wir ziehen um in die Seilerstraße!'
date: 2017-02-22T21:38:00+06:00
featureImage: /blog/wir-ziehen-um-in-die-seilerstrasse/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "2017", "seilerstraße"]
---

Nichts ist so beständig wie der Wandel ... oder so. Wie auch immer, manch einer hat es schon gehört: das z-Labor ist mal wieder umgezogen. Diesmal haben wir einen schönen Raum in einem alten Indutriegebäude in der Seilerstraße gefunden:

![](/blog/wir-ziehen-um-in-die-seilerstrasse/img_1384.webp)

Eingang an der Vorderseite, wir sitzen direkt obendrüber ...

![](/blog/wir-ziehen-um-in-die-seilerstrasse/fullsizerender.webp)

das neue z-Labor von innen:

![](/blog/wir-ziehen-um-in-die-seilerstrasse/img_1321.webp)

Auf unserer Seite findet ihr noch eine kleine Anfahrtsskizze. Wir treffen uns nachwievor mittwochs ab 19:30 Uhr.
---
title: 'Umzug'
date: 2023-05-01T01:25:00+06:00
featureImage: /blog/umzug/thumbnail.jpeg
tags: ["hackerspace", "makerspace", "zwickau", "2023", "kulturweberei", "mastodon"]
---

## Wir ziehen um: Mehr Platz fürs Hacken und Schnacken


Wir haben unsere Räumlichkeiten nach wie vor in der Kulturweberei der Seilerstraße 1. Allerdings findet ihr uns ab Ende des Monats nicht mehr in Box 26, sondern in **Box 39** (Treppe hoch links).

Wir sind schon mittendrin in den Vorbereitungen, **daher fallen die nächsten beiden offenen Donnerstage aus.** Aktuelles findet ihr zudem im Fediverse unter: [chaos.social/@zLabor](https://chaos.social/@zLabor)

Hier seht ihr die ersten Impression. Es wird großartig \o/

#### Blick auf den Raumteiler und den hinteren Teil des neuen Raumes:
![](/blog/umzug/Umzug1.jpeg)

#### Blick auf die Kletterwand und Hochkontruktion:
![](/blog/umzug/Umzug2.jpeg)

#### Mollton an der Decke:
![](/blog/umzug/Umzug3.jpeg)


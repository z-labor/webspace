---
title: 'Das war der zweite Linux-Tag im z-Labor'
date: 2019-12-01T00:58:00+06:00
featureImage: /blog/das-war-der-zweite-linux-tag-im-z-labor/thumbnail.webp
tags: ["hackerspace", "makerspace", "2019", "lpd", "linux", "opensource", "zwickaugehtauchanders"]
---

Welche Linux-Distributionen laufen am besten auf alten Rechnern? Wie kann man Windows-Spiele unter Linux zum Laufen bringen? Wie lassen sich Multiboot-Probleme lösen und druckt dieser elende Drucker mit dem uralten Treiber? Mit diesen und vielen weiteren Anliegen haben zum [Linux Presentation Day](https://l-p-d.org/de/start) (LPD) am 16. November mehr als 20 Gäste den Weg zu uns ins z-Labor gefunden.

Bereits zum zweiten Mal in diesem Jahr nutzten wir diesen nicht-kommerziellen Aktionstag, um über die Vor- und Nachteile verschiedener Linux-Varianten zu informieren. Außerdem hatten wir den Anspruch die gesamte Welt der freien Software auch in Zwickau und Umgebung ein Stück weit bekannter zu machen.

![](/blog/das-war-der-zweite-linux-tag-im-z-labor/dsc_0803-3.webp)

Dabei war das Publikum sehr gemischt und reichte von engagierten Senioren, über neugierige Jugendliche bis hin zu Menschen, die noch gar nicht mit freier Software in Berührung gekommen sind. Somit war auch der zweite Linux Presentation Day im z-Labor ein voller Erfolg. Wir werden allen interessierten Kreaturen auch bei unserem dritten LPD im Jahr 2020 wieder charmant und kompetent zur Seite stehen.

![](/blog/das-war-der-zweite-linux-tag-im-z-labor/dsc_0795.webp)
![](/blog/das-war-der-zweite-linux-tag-im-z-labor/dsc_0824-1.webp)

Freie Software für eine freie Gesellschaft!
---
title: 'Ideenwettbewerb Zwickau: Wir haben gewonnen!'
date: 2016-06-14T21:19:00+06:00
featureImage: /blog/preistraeger-ideenwettbewerb/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "ZwickauGehtAuchAnders", "2016", "ideenwettbewerb"]
---

Wir sind tatsächlich prämiert worden!!! Aus unseren fünf eingereichten Ideen beim[ Wettbewerb der Stadt Zwickau](http://www.zwickau.de/de/politik/aktuelles/ideenwettbewerb.php?shortcut=ideenwettbewerb) sind wir mit zweien ausgezeichnet worden.die Jury hat die beiden Ideen Kleingartenanlage 2.0 und Grüne Wochen gemeinsam als wertvoll gesehen und uns mit einem Preis von 200€ belohnt. Zur Preisverleihung haben wir eine Urkunde und eine Sonneblume entgegennehmen dürfen. Außerdem wurde ein Poster mit unserer Idee erstellt zund präsentiert.!

![](/blog/preistraeger-ideenwettbewerb/dsc06276.webp)

Neben uns wurden noch ganz viele andere wunderbare Ideen ausgezeichnet. Nun ist es an uns Paten für unsere Ideen zu finden, um sie möglichst bald in die Tat umzusetzen. Ihr werdet also sicher bald wieder von uns hören 😉

![](/blog/preistraeger-ideenwettbewerb/dsc06279.webp)
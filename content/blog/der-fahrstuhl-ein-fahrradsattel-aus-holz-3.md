---
title: 'Der FahrStuhl: ein Fahrradsattel aus Holz #3'
date: 2017-08-18T22:29:00+06:00
featureImage: /blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-3/thumbnail.webp
tags: ["hackerspace", "makerspace", "2017", "projekte"]
---

Nachdem wir im ersten Teil schon mal das Ergebnis und im letzten Beitrag den Unterbau des FahrStuhls vorgestellt haben, geht es in diesem Teil um die eigentliche Form und den Zusammenbau.

Die Form des Sattels an sich habe ich in dieser Version komplett digital erstellt. Nachdem ich bei Version 0.1 noch die Form eines existierenden Sattels per Hand kopiert habe, habe ich diesmal etwas Software geschrieben. Das Ziel war es dabei einen Code zu entwickeln, der nach Eingabe einiger Grundparameter wie Länge und Breite, die Form des Sattels sowie alle nötigen Schichten errechnet und als Datensatz für das spätere Schneiden der Schichten auf einem Lasercutter bereitstellt.

Ich habe dafür die Form des Sattels abstrahiert und in drei Teile aufgeteilt, die ich nach einigem Experimentieren wie folgt approximiert habe:

* Kreis bzw. Ellipse für den Teil auf dem man hauptsächlich sitzt
* Kreis für das andere, schmale Ende
* eine Parabel der Form y = mx² + n um beide Teile zu verbinden.

Die Parabel hat dabei ihren Nullpunkt im Scheitelpunkt des kleineren Kreises.

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-3/web_grundform_sattel.webp)

Der Schnittpunkt mit dem großen Kreis ist sehr wichtig um eine möglichst kontinuierliche  Außenform zu generieren. Mathematisch lässt sich das lösen in dem man auf die Tangentenstetigkeit der beiden Funktionen Kreis und Parabel achtet. Man kann beispielsweise die Ableitung beider Funktionen bilden und damit den Punkt bestimmen an dem beide gleich bzw. sehr ähnlich sind. Im Code den ich schließlich umgesetzt habe, habe ich die einfache Variante gewählt und einen fixen Wert für den Kreuzungspunkt gewählt. Je nach Änderung der Parameter für Länge und Breite des Sattels kann man dann den Schnittpunkt anpassen bis die Form (für den eigenen Geschmack) gut passt. Wer möchte kann die bessere Variante gern noch im Code implementieren. Den Link zum Projekt findet ihr am Ende des Posts.

Die korrekte Form des Sattels erhält man nun sehr einfach in dem man die Koordinaten der jeweiligen Elemente immer bis zum nächsten Schnittpunkt kombiniert und dann noch eine invertierte Kopie (bisher hat der Code ja nur eine Hälfte der Kontour erstellt) hinzufügt. Das sieht dann in etwa so aus:

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-3/web_grundform_sattel_2.webp)

Mit Hilfe dieses Plots kann man sich eine individuelle Grundform des Sattels entwickeln und mit verschiedenen Größen spielen. Ich habe mir, um eine passende Form zu finden, verschiedene Varianten berechnet und diese jeweils entweder ausgedruckt oder auf einem Lasercutter zunächst aus Papier ausgeschnitten. Damit konnte ich im Vergleich zu anderen Sätteln einschätzen was eine passende Form sein könnte.

Der nächste wesentliche Schritt ist die Erzeugung der einzelnen Lagen um eine dreidimensionale Form zu erhalten. Hierzu habe ich zunächst auf Grundlage des Sattels, den ich bereits per Hand hergestellt habe, eine Zielhöhe und mit der Dicke des Furniers, das ich zur Verfügung habe, eine Lagenanzahl bestimmt. Bei mir waren das 13 Lagen zu ca. 0,8 mm. Ebenfalls am alten Sattel habe ich bestimmt wie sich die Lagen nach oben hin verkleinern. Bei mir hat sich eine logarithmische Funktion ergeben, deren Parameter ich mir durch einen Fit bestimmt habe. Im Code bin ich dann folgendermaßen vorgegangen:

* Anstieg jedes einzelnen Punktes der Grundform bestimmen
* die Orthogonale darauf berechnen
* den Schnittpunkt der Orthogonalen mit der Grundlinie bestimmen
* daraus die Länge der Verbindungslinie bestimmen
* die Länge entsprechend der logarithmischen Funktion kürzen
* die daraus neu entstehenden Koordinate ermitteln.

Damit kann man sehr einfach beliebig viele verkleinerte Lagen errechnen. Momentan programmiere ich noch eine Variante, die die Verkleinerung anhand der Polarkoordinaten berechnet. Davon verspreche ich mir noch eine Vereinfachung des Prozesses.

Ein Problem bei der Erstellung der Lagen ist, dass ab einer gewissen Höhe die Verkleinerung für den schmalen Teil des Sattels nicht mehr möglich ist, weil dieser dann zu klein ist.

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-3/web_kreuzung_layer.webp)

Bei den Abmaßen, die ich in diesem Fall gewählt hatte, war das ab Lage 12 der Fall. Aus den Erfahrungen des ersten Sattels wusste ich auch, dass ich ein bis zwei Lagen ohne das lange Ende generieren muss um beim Schleifen eine ausreichend runde Form an der Sitzfläche zu erhalten. Macht man das nicht, sitzt man buchstäblich wie auf einem Brett.

Die Programmierung dieser Extraschichten basiert auf den bereits errechneten, aber am langen Ende unpassenden Schichten. Ich habe dafür eine Länge eingeführt, bei der die original erzeugte Schicht enden soll. Bis zu diesem Punkt habe ich die originalen Koordinaten verwendet. Die Höhe des letzten Punkts der Parabel gibt den Radius des Kreises vor, der den neuen Abschluss bilden soll. Auch hier habe ich die „Tangentenstetigkeit“ der Einfachheit halber zunächst nicht berechnet sondern einfach per Hand angepasst. Für den Sattel den ich schließlich gebaut habe, habe ich zwei dieser Schichten berechnet.

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-3/web_alle_lagen_sattel.webp)

Letztendlich habe ich die Daten für jede Schicht in eine SVG-Datei geschrieben, damit ich sie in einem Lasercutter laden und aus Furnierholz ausschneiden kann. Ich habe den ganzen Code in [GNU Octave](https://www.gnu.org/software/octave/), einer OpenSource Alternative zu Matlab, geschrieben. Die dort implementierte Exportfunktion für SVG Dateien hat für mich leider nicht funktioniert, weshalb ich mir eine eigene Funktion geschrieben habe. Dadurch, dass [SVG auf XML](https://de.wikipedia.org/wiki/Scalable_Vector_Graphics) basiert ist das sehr einfach.

Den Sattel habe ich dann in zwei Schritten hergestellt. Zuerst habe ich die ersten 6 Lagen und die weiteren 7 Lagen miteinander verklebt. Aus den ersten 6 Lagen habe ich in der Mitte einen beträchtlichen Teil wieder ausgeschnitten (auf Basis der Lage 12, die ich nur dafür benutze). Damit dienen diese Lagen nur als Rahmen und haben keine tragende Funktion. Außerdem ist der Rahmen gerade so hoch, dass der Unterbau damit bündig abschließt, was optisch sehr schön wirkt. Nachdem ausschneiden habe ich dann beide Hälften und danach den Unterbau zusammengeklebt. Dabei habe ich das C-Profil an den neuen Sattel (FS v02.00) und das S-Profil an den alten Sattel (FS v01.00) geklebt.

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-3/foto-14-06-17-21-27-35.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-3/foto-14-06-17-21-27-53.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-3/foto-19-06-17-18-14-23.webp)

Am Ende musste ich den Sattel „nur noch“ in Form schleifen ;-). Da ich aus Vorsicht das ganze zunächst mit der Hand und einer groben Feile gemacht habe, hat sich das noch eine Weile hingezogen. Am Ende habe ich die Oberfläche mit etwas Schleifpapier glatt geschliffen. Eine finale Lackierung fehlt momentan noch, wird aber noch kommen.

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-3/foto-19-06-17-18-32-49.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-3/foto-19-06-17-18-32-56-1.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-3/foto-19-06-17-18-23-18.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-3/foto-24-06-17-20-01-18.webp)

Nach einem kleinen Fotoshooting  bin ich den Sattel natürlich auch schon gefahren. Zunächst noch sehr vorsichtig, fahre ich mittlerweile ganz normal damit. Ob in der Stadt oder auf der Landstraße, ob auf Asphalt oder Pflaster, der Sattel bzw. der Unterbau federt ganz gut mit und macht eine angenehme Fahrt möglich. Natürlich ist die Oberfläche hart, aber durch die Federung werden schon einige Unebenheiten ausgelichen. Auch auf nach einer Tour mit insgesamt 130 km habe ich keine Schmerzen verspürt. Alle Testfahrer, die bisher gefahren sind, waren positiv überrascht von der Fahrbarkeit.

Den Quellcode für die Sattelform findet ihr [hier](https://github.com/chrs-taudt/FahrStuhl-code) auf GitHub.

Wer sich also traut, kann gern mittwochs ab 19:30 Uhr im z-labor vorbeikommen.
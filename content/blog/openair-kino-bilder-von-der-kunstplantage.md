---
title: 'Openair Kino: Bilder von der Kunstplantage'
date: 2019-07-21T00:13:00+06:00
featureImage: /blog/openair-kino-bilder-von-der-kunstplantage/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "zwickaugehtauchanders", "2019", "veranstaltung", "kino", "kunstplantage", "ccc"]
---

Trotz einiger verlorener Regentropfen hatten wir eine gemütliche Kinovorführung auf der Kunstplantage Zwickau mit dem Film „[all creatures welcome](https://sandratrostel.de/acw/)“ von [Sandra Trostel](https://sandratrostel.de/). Neben Gesprächen über die Hackerkultur und einige konkrete Projekte haben wir auch neue Kontakte zu Interessierten geknüpft. Im Folgenden seht ihr einige Bilder vom Abend:

![](/blog/openair-kino-bilder-von-der-kunstplantage/dsc_0586.webp)
![](/blog/openair-kino-bilder-von-der-kunstplantage/dsc_0583.webp)
![](/blog/openair-kino-bilder-von-der-kunstplantage/dsc_0592.webp)
![](/blog/openair-kino-bilder-von-der-kunstplantage/dsc_0577.webp)
![](/blog/openair-kino-bilder-von-der-kunstplantage/dsc_0588.webp)
---
title: 'Der FahrStuhl – ein Fahrradsattel aus Holz #2'
date: 2017-07-26T22:27:00+06:00
featureImage: /blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-2/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "projekte", "2017"]
---

Es ist schon eine Weile her, dass wir über den Holzsattel von [Christopher](https://twitter.com/ch_taudt) geschrieben haben. Während wir im ersten Teil schon die Bilder bestaunen durften, sehen wir uns in Teil 2 und Teil 3 die technischen Details etwas genauer an.

Vor allem der untere Teil des Sattels, der für die nötige Federwirkung sorgen soll, hatte beim ersten Versuch nicht gehalten. Demzufolge habe ich an der Stelle zuerst weiter gearbeitet. Während ich im ersten Versuch noch einen bogenförmigen Unterbau gebaut hatte, der direkt auf die Sattelstange geschraubt war, habe ich für den zweiten Versuch auf eine C- und eine S-Form gesetzt, die jetzt geklemmt werden sollten. Vor allem die Bohrung durch das Holz und die Holzssorte waren beim ersten Versuch Schwachstellen. Zuerst habe ich für beide Unterbauten ein Werkzeug zum Kleben einzelner Furnierholzlagen gebaut.

Die Maße dafür habe ich am alten Sattel direkt abgenommen und vor allem mit Hinblick auf die entstehenden Hebelarme erstmal nur grob entworfen. So hat sich ergeben, dass das C-Profil einen Radius von 50 mm und final eine Armlänge von ca. 45 mm hat. Beim S-Profil sind die Radien 50 und 40 mm sowie die Armlängen 45 und 30 mm. Für die Materialwahl habe ich mich wieder im Bereich Skateboards orientiert, weil das aus meiner Sicht eine der wenigen Anwendungen für Holz ist, bei denen ähnlich hohe dynamische Belastungen auftauchen. In dem Bereich ist der sogenannte seven-ply berühmt. Das ist der Lagenaufbau des Skateboards, der im wesentlichen aus sieben Lagen Ahornholz (engl. maple) besteht. Ich habe also auch auf Ahornholz gesetzt. Da ich aber mit einem vergleichweise kleinen Biegeradius arbeiten musste/wollte, habe ich auf sehr dünne Furnierstreifen mit 0,6 mm Stärke gesetzt. Als Dicke der späteren Struktur habe ich ca. 10 mm angepeilt. In die eigentliche Form haben dann um die 20 Lagen des Furniers gepasst. Ich habe die Lagen zunächst alle trocken ins Werkzeug eingelegt und den Arbeitsablauf zum Kleben getestet. Später habe ich dann alle Lagen einzeln, dünn mit einem Ein-Komponenten Polyurethan Kleber bestrichen und mit Schraubzwingen ins Werkzeug gepresst.

![Test](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-2/foto-09-01-17-21-00-27.webp "Test")

Ich habe alle Lagen trocken und nicht vorgeformt verklebt. Die Lagen sind so dünn, dass ich sie nicht mit Dampf oder etwas ähnlichem vorbiegen musste. Auch der Kleber war ausreichend um den Verbund auch nach dem Verkleben in Form zu halten. Die geklebten Unterbauten habe ich nach dem Entformen noch passend zurecht geschnitten und geschliffen.

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-2/foto-13-01-17-08-13-52.webp)

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-2/foto-13-01-17-08-13-25.webp)

Um die Unterbauten mit der Sattelstange zu verbinden habe ich einen Adapter aus Aluminium entworfen, der das Holz in einer T-Nut aufnehmen und dann mit wenigen Schrauben klemmen kann. Durch zwei eingelegte Alu-Bleche wird die Punktlast der Schrauben auf das Holz flächig verteilt. Außerdem lässt sich mit Hilfe der Bleche eine unterschiedliche Dicke der Unterbauten ausgleichen.

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-2/detail_klemmung.webp)

Damit sollte der Unterbau stabil genug an der Sattelstange zu befestigen sein und ausreichend Federwirkung beim Fahren geben. Im nächsten Beitrag geht es dann um die Form des eigentlichen Sattels und den Zusammenbau sowie die erste Testfahrt.


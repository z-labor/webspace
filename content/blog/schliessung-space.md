---
title: 'Schließung Space'
date: 2020-03-16T01:25:00+06:00
featureImage: /blog/schliessung-space/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "2020", "corona", "irc"]
---

Aufgrund der aktuellen Situation bleibt der Space für öffentliche Besuche/Veranstaltungen bis auf weiteres geschlossen. Aber natürlich treffen wir uns dauerhaft online vor allem im [IRC](irc://irc.hackint.eu/#z-labor). Schaut gern vorbei und bleibt gesund!
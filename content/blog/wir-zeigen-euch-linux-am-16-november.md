---
title: 'Wir zeigen euch Linux am 16. November'
date: 2019-10-22T00:52:00+06:00
featureImage: /blog/wir-zeigen-euch-linux-am-16-november/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "2019", "linux", "linuxpresentationday", "lpd", "veranstaltung", "opensource"]
---

Wir nehmen wieder am [Linux Presentation Day](https://l-p-d.org/:en:start) (LPD) teil! Zum zweiten Mal in diesem Jahr präsentieren wir euch am Samstag, den **16. November** 2019, wie leicht es ist auf Linux umzusteigen und beantworten all eure Fragen rund um das freie Betriebssystem und Open Source Software generell.

Der LPD ist eine Art Tag der offenen Tür, der weltweit stattfindet. Institutionen wie Schulen, Hackerspaces oder Anwendergruppen, die sich mit Freier Software und ähnlichen Themen beschäftigen, öffnen an diesem Tag ihre Räumlichkeiten, um allen Interessierten das Linux-Universum näher zu bringen. Einen kleinen Eindruck vom ersten LPD in diesem Jahr bekommt ihr in unserem Rückblick.

Ihr könnt gern einen Computer mitbringen oder einfach so vorbeikommen und erfahren, wie einfach die Installation ist, welche Programme euch für den Alltag oder ganz spezielle Aufgaben zur Verfügung stehen und was sich neben einem Desktopcomputer/Laptop noch mit Linux betreiben lässt.

Besucht uns  am 16.11.** zwischen 11:00 und 19:00 Uhr** im z-Labor, Seilerstraße 1, 08056 Zwickau! USB-Sticks mit verschiedenen Linuxsystemen liegen schon zum Testen für euch bereit. 🙂

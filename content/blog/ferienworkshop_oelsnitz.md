---
title: 'Ferienworkshop für Nachwuchsbastler'
date: 2024-07-18T09:30:00+06:00
featureImage: /blog/ferienworkshop/thumbnail.webp
tags: ["hackerspace" , "lötworkshop" , "oelsnitz" , "bergbaumuseum" , "bristlebot" , "kinderlöten" , "diy" , "2024"]
---

## Motorisierte Bürstentiere für alle
Am 24.07.24 veranstalten wir einen Löt-und Bastelworkshop während des Ferienprogramms im [Bergbaumuseum Oelsnitz](https://www.bergbaumuseum-oelsnitz.de).

Wir freuen uns auf die Möglichkeit gemeinsam mit Kindern kleine Roboter aus Zahnbürstenköpfen, Motoren, Pfeifenreinigern und Wackelaugen zu bauen, die auch [Bristlebots](https://en.wikipedia.org/wiki/Bristlebot) genannt werden.

Dadurch haben die Kids die Chance auf spielerische sowie kreative Art und Weise Erfahrungen mit Lötgeräten und Elektronikkomponenten zu machen und am Ende ihre eigenen kleinen Robo-Bürstentiere in einer Arena gegeneinander antreten lassen.

Vielen Dank für das große Interesse an unserem Angebot. Wir bedauern, dass wir nicht alle Gruppen aufnehmen konnten. 

Im Folgenden ein paar Bilder vom Druck der Gehäuse, der Prototypen sowie von zwei fertigen Exemplaren:
![3D-gedruckte Gehäuse für die Zahnbürstenroboter](/blog/ferienworkshop/gehäuse.jpeg)
![zwei Prototypen der Bristlebots](/blog/ferienworkshop/bristlebots.webp)
![bristlebots gestaltet](/blog/ferienworkshop/bots.webp)

## Weitere Infos unter:
https://www.bergbaumuseum-oelsnitz.de/veranstaltungen/jahresprogramm/434.html?day=20240724&times=1721806200,1721815200

Aktuelles findet ihr zudem auf Chaos.Social unter: [chaos.social/@zLabor](https://chaos.social/@zLabor)
---
title: 'Start frei!'
date: 2015-01-15T21:07:00+06:00
featureImage: /blog/start-frei/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "2015"]
---

Betreutes Basteln auf aller niedrigstem Niveau - unter diesem Credo haben wir uns zusammen gefunden, um auch in Zwickau einen [Hacker- und Maker-Space](https://de.wikipedia.org/wiki/Hackerspace) zu starten. Im Moment befinden wir uns noch in der Einrichtungs- und Aufbauphase. Bilder davon und von der Location (eine alte und momentan kalte Halle) folgen noch. Da wir noch keine festen Strukturen und keinen Masterplan haben, sondern einafch nur loslegen wollen, gibt es auch noch keine festen Regeln. 

Wir treffen uns meist an Mittwochabenden. Wer Lust hat vorbeizuschauen und/oder mitzumachen kann uns einfach anschreiben unter [info@z-labor.space](info@z-labor.space) oder uns per [Twitter](https://twitter.com/z_labor) finden und folgen.

Weitere Infos und Bilder folgen!
---
title: 'Openair Kino „all creatures welcome“'
date: 2019-07-12T00:03:00+06:00
featureImage: /blog/openair-kino-all-creatures-welcome/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "zwickaugehtauchanders", "2019", "veranstaltung", "kino", "kunstplantage", "ccc"]
---

Wir versuchen uns als Kartenabreißer und präsentieren euch zusammen mit der [Kunstplantage Zwickau](https://www.facebook.com/kunstplantagezwickau/) den Film über die Hacking-Szene [all creatures welcome](https://sandratrostel.de/acw/). Am [19.07.2019 ab 20 Uhr](https://de-de.facebook.com/events/475757683228797/) ist es soweit.

Während alle von Digitalisierung und diesem Internet sprechen, gibt es schon seit über dreißig Jahren eine sehr lebendige Szene aus Nerds, Hacker.innen und Aktivist.innen, die sich mit Computern und Technologie auf kreative Weise beschäftigen.

Der [Chaos Computer Club](https://www.ccc.de/) war dabei mit seinen Veranstaltungen und Aktionen einer der Vorreiter. Obwohl für viele der Begriff „Hacker“ negativ besetzt ist, zeigt sich mit dem größten europäischen Hackerverein, dass der Schutz und die Vernetzung aller kreativer Kreaturen wichtig ist. Denn Tatsächlich prägen Sie unsere Gesellschaft mehr, als die meisten ahnen.

[Sandra Trostel](https://sandratrostel.de/) hat einige Hacking-Veranstaltungen besucht und versucht in ihrem Dokumentarfilm nicht nur mit Mythen aufzuräumen, sondern vor Allem die Vielfalt einer Szene zu zeigen, die allzu schnell auf Kapuzenpulli tragende Pizza-Jungs verkürzt wird. In knapp 90 Minuten zeigt sie die Bandbreite der Hackercommunity und bringt rüber um was es geht: Kreativität, Witz, Neugier an Technik und Respekt im Umgang mit einander.

Mit dem Film laden wir euch ein, euch einer alten und doch meist unverstandenen Subkultur zu öffnen.

Eintritt: Frei
Getränke sind am Start 😉 

![](/blog/openair-kino-all-creatures-welcome/acw_poster_ki_regenbogen_digital-e1541798837858.webp)

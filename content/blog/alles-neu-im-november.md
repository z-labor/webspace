---
title: 'Alles neu im November'
date: 2015-11-29T18:29:00+06:00
featureImage: /blog/alles-neu-im-november/thumbnail.webp
tags: ["hackerspace", "makerspace", "2015"]
---

Nachdem wir aus dem Abenteuerspielplatz in der Schlachthofstraße im Sommer ausgezogen sind und scheinbar ewig nach einem neuen Raum gesucht haben, sind wir jetzt in einer Garage gelandet. An der Leipziger Straße in Pölbitz haben wir es uns nun auf nur wenigen Quadratmetern gemütlich gemacht.

![](/blog/alles-neu-im-november/thumbnail.webp)

Neben zwei Werkbänken und einem Lötbereich haben wir auch schon das Sofa hingepackt. Ein Tisch und diverse Stühle sind auch schon da. Demnächst wird es eine Leinwand und einen Beamer geben, sodass wir auch für kleine Vorträge und ähnliches gerüstet sind. Alles in Allem ist eher gemütlich, aber für einigen Blödsinn geeignet. Schaut doch einfach vorbei: zum Beispiel jeden Mittwoch ab 19 Uhr, Leipziger Straße 147 (hinter Musik Schiller). Gerne könnt ihr auch einfach so vorbeikommen, dazu schreibt ihr uns am besten über twitter ([@z_labor)](https://twitter.com/z_labor) oder per Mail an [info@z-labor.space](info@z-labor.space).

Wir feilen seit kurzem auch an einem Logo für den Hackerspace. Die folgenden Entwürfe sind bisher dabei rausgekommen:

![](/blog/alles-neu-im-november/logo_1.webp)
![](/blog/alles-neu-im-november/logo_2.webp)
![](/blog/alles-neu-im-november/logo_3.webp)
![](/blog/alles-neu-im-november/logo_4.webp)

Ihr habt eine Meinung dazu oder selber einen Vorschlag? Immer her damit!! Schreibt uns oder kommt ebenfalls vorbei.

Übrigens sind wir auch seit neustem ein eingetragener Verein. Ihr könnt natürlich wie üblich einfach so bei uns vorbeikommen, quatschen, basteln, abhängen oder hacken wie ihr wollt. Oder ihr macht all das und bezahlt dafür einen kleinen Beitrag, damit wir weiter Miete und Werkzeug zahlen können…. ;-).
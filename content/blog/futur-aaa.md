---
title: 'Futur AAA Festival'
date: 2024-06-09T13:37:00+06:00
featureImage: /blog/futur-aaa/thumbnail.webp
tags: ["hackerspace", "zwickau", "2024", "festival", "kunstplantage", "projekt46" , "livecoding" , "livepoetry" , "zwickaugehtauchanders" , "generativeart" , "algorave"]
---

## Fesival für elektronische Musik und Kunst am 14.-16.Juni 

Am zweiten Juni-Wochenende präsentieren sich beim ersten Futur-AAA-Festival in
Zwickau Künstler:innen aus mehreren Ländern und verbinden Hörbares und Sichtbares zu immer wieder neuen
Mustern. Das Event ist ein offizieller Satellit der renommierten International Conference on Live Coding
(ICLC) in Shanghai und bringt damit internationale Größen und neue Talente zusammen.

### Was ist Live Coding?
Live Coding ist eine Kunstform, bei der Künstler:innen in Echtzeit Algorithmen schreiben, um
Musik oder Visuals zu generieren – also akustische oder visuelle Muster. Diese einzigartige Form
des Ausdrucks bietet dem Publikum die Möglichkeit, den kreativen Prozess des Entstehens live
mitzuerleben. 

### Programm und Highlights

Das Festival beginnt am Freitagabend mit dem Film "Sounds of Code", der einige Live
Coder:innen porträtiert. Anschließend nimmt das spanische Künstlerkollektiv [PlantaVibras](https://www.plantavibras.com/) den Raum und das Publikum mit in eine interaktive Performance, um die Vielfalt des Live Codings erfahrbar, hörbar und sichtbar zu machen. Die Eröffnung des Festivals findet im [Projekt 46](https://www.instagram.com/projekt46_) statt.

Am Samstag finden tagsüber verschiedene Workshops statt, die einerseits Live-Coding-Praktiken erklären, sich andererseits mit dem DJ-ing beschäftigen. Am Abend erwartet die Gäste auf der [Kunstplantage Zwickau](https://www.instagram.com/kunstplantagezwickau/) ein abwechslungsreiches Programm aus Techno, Live Coding und Visuals bei einem Algorave. Insgesamt werden 11 Künstler:innen aus 5 Ländern ihre Arbeiten vorstellen. Zu den besonderen Gästen gehören renommierte Künstler wie [Bertolt Meyer](https://www.instagram.com/bertolt01) und [Flor de Fuego](https://flordefuego.github.io/bio_eng.html). Der eine ist Professor an der TU Chemnitz, forscht zu Verschmelzung von Mensch und Technik ganz unmittelbar und erschafft seine ganz eigene Musik. Die andere ist Professorin an der Universität von La Plata in Argentinien und gibt Meyers Musik mit ihren ebenfalls live programmierten Visuals ein Aussehen.

![](/blog/futur-aaa/p46.png)

![](/blog/futur-aaa/kunsti.png)

### Kontakt und weitere Infos 

Ein Wochenendticket für das gesamte Festival, inklusive Camping auf der Kunstplantage, kostet 30
Euro. Dies ermöglicht den Zugang zu allen Veranstaltungen und Workshops sowie die Teilnahme an
den Performances und Networking-Möglichkeiten. Aber keine Sorge, wenn das finanziell nicht möglich ist - einfach vorbei kommen und "Pay What You Want or Can"! 

* Web: https://futur-aaa.com
* Mail: info@futur-aaa.com
* Instagram: https://www.instagram.com/futur.aaa
* Mastodon: https://social.toplap.org/@futurAAA



![](/blog/futur-aaa/musikfonds.jpg) ![](/blog/futur-aaa/KDFS.png)


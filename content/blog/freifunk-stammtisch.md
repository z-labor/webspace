---
title: 'Freifunk Stammtisch'
date: 2018-07-01T23:26:00+06:00
featureImage: /blog/freifunk-stammtisch/thumbnail.webp
tags: ["hackerspace", "makerspace", "freifunk", "zwickau", "2018", "zwickaugehtauchanders"]
---

[Freifunk](https://freifunk.net/) ist eine Initiative zum dezentralen Aufbau von freien Netzen in denen zum Beispiel Menschen anderen den Zugang zum Internet zur Verfügung stellen.

Auch in Zwickau beteiligen sich mittlerweile einige am Aufbau eines freien Netzes und Teilen die eigene Internetverbindung. Beispielsweise kommt man schon in der Bahnhofstraße, Teilen der Hauptstraße, am Neumarkt, am Kornmarkt oder auf dem Platz der Völkerfreundschaft frei ins Internet (Auf dieser [Karte](https://meshviewer.chemnitz.freifunk.net/) mal nach Zwickau scrollen).

Zur besseren Vernetzung, zur Kommunikation der Freifunk-Ideen und zur Beratung bei ganz konkreten Fragen bietet die Freifunkgruppe in Zwickau ab jetzt einen monatlichen Stammtisch an. **Immer am ersten Mittwoch im Monat** stellen wir vom z-Labor unsere Räume für den Freifunk-Stammtisch zur Verfügung.

**Wer kann und sollte kommen?**
* Jeder der nicht weiß was Freifunk ist und sich gern informieren möchte.
* Jeder der weiß was Freifunk ist und sich gern einbringen möchte (z.B. durch Aufstellen eines Knotens, Teilen des Freifunk-Gedankens, Gestalten von Infomaterial, Programmieren an der Firmware, etc. )
* Jeder der schon an Freifunk beteiligt ist und sich über Aktuelles austauschen möchte.
* Jeder der schon weiß was Freifunk ist, jetzt loslegen möchte, aber keinen Plan hat wie es geht.

**Hard Facts:** 

* Freifunkstammtisch ab Mittwoch, 04.07.2018 ab 20 Uhr 
* Seilerstraße 1 
* z-Labor, Hacker- und Makerspace ([Karte](https://zlabor.files.wordpress.com/2015/01/anfahrt_zlabor_mit_markierung.webp?w=601&h=379))
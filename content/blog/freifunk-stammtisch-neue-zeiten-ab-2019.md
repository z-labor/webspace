---
title: 'Freifunk Stammtisch – Neue Zeiten ab 2019'
date: 2019-03-25T23:39:00+06:00
featureImage: /blog/freifunk-stammtisch-neue-zeiten-ab-2019/thumbnail.webp
tags: ["hackerspace", "makerspace", "freifunk", "zwickau", "2019"]
---

Seit dem letzten Jahr treffen sich bei uns [Freifunk](https://freifunk.net/)-Interessierte zum Austausch und zur Diskussion rund um Knoten, Router, Gluon, etc. In diesem Jahr ändert sich die Zeit des Stammtischs. Ab dem 01.04.2019 findet der Stammtisch immer am **ersten Montag im Monat ab 19:00 Uhr** in unseren Räumen in der Seilerstraße 1 statt.

**Die Termine für 2019**:

* 01.04. (kein Scherz 😉)
* 06.05.
* 03.06.
* 01.07.
* 05.08.
* 02.09.
* 07.10.
* 04.11.
* 02.12.
---
title: 'Offener Abend nun Donnerstags'
date: 2022-11-02T17:03:00+06:00
featureImage: /blog/offener-abend-donnerstags/thumbnail.jpg
tags: ["hackerspace", "makerspace", "2022"]
---

Ab sofort findet unser offener Abend & #Chaostreff immer donnerstags ab 19:00 Uhr statt.

Tatort: Seilerstraße 1, Haus C, Box 26
---
title: 'Ideenwettbewerb Zwickau #3: Grüne Wochen'
date: 2016-04-06T15:54:00+06:00
featureImage: /blog/ideenwettbewerb-zwickau-3-gruene-wochen/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "ZwickauGehtAuchAnders", "2016", "ideenwettbewerb"]
---

Schlag auf Schlag, hier ist schon unsere dritte Idee zum gerade abgelaufenen [Zwickauer Ideenwettbewerb](http://www.zwickau.de/de/politik/aktuelles/ideenwettbewerb.php?shortcut=ideenwettbewerb). Haben wir euch eigentlich schon verraten, dass wir insgesamt fünf Ideen und zwei Outtakes haben? Diesmal geht es darum die Stadt gemütlicher zu machen um es für alle angenehmer zu machen das Buch mit dem Kaffee in der Hand auf dem Hauptmarkt zu verschlingen oder so. Also dann: Rollrasen raus, Sand aufgeschüttet und Wasser ins Planschbecken. Zwickau will mehr Gemütlichkeit und Leben in der Stadt. So stehts dann auch in unserem Wettbewerbstext:

> _Die Stadt braucht Leben? Die Stadt braucht in erster Linie mehr Gemütlichkeit! Eine Stadt in der sich Menschen gern aufhalten beherbergt nicht nur Geschäfte, Institutionen und Restaurants. Eine lebendige Stadt lädt vor allem ihre Besucher mit Gemütlichkeit zum Bleiben ein._
> 
> _Diese Idee besteht aus der Begrünung von Flächen in der Innenstadt wie dem Hauptmarkt, der Hauptstraße und dem Kornmarkt. Die konkret geplanten Flächen sind in der unten stehenden Skizze markiert. Diese Bereiche könnten mit Hilfe von Rollrasen temporär begrünt werden. Außerdem soll auf dem Hauptmarkt ein Bereich für Beachvolleyball und eine Chill-Out Area entstehen. Außerdem soll im markierten Bereich (zum Beispiel am Kornmarkt) ein Bereich mit mobilen Swimming-pools bzw. Planschbecken entstehen. Diese Anlagen würden mehr Leute animieren sich in der Stadt auf ein Stück Wiese zu setzen, vielleicht einen Cocktail zu trinken, ein Eis zu essen oder ein Buch zu lesen. Mit Hilfe von diesen kleinen Maßnahmen würde die Stadt sofort gemütlicher und die angrenzende Gastronomie könnte außerdem noch profitieren._
> 
> _Die Idee könnte zunächst als kurzzeitige Veranstaltung (zum Beispiel 1-2 Wochen) im Sommer stattfinden um die Resonanz zu testen. Bei Erfolg könnte man den Zeitraum ausdehnen oder über eine permanente Begrünung nachdenken._
> 
> _Die Skizze zeigt die Flächen die durch eine befristete Begrünung für mehr Gemütlichkeit in der Innenstadt sorgen sollen._
> 
> ![](/blog/ideenwettbewerb-zwickau-3-gruene-wochen/grc3bcnes_zwickau_v1.webp)
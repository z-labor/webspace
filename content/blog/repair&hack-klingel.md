---
title: 'Hack & Repair: SIP-Türklingel'
date: 2025-02-03T00:52:00+06:00
featureImage: /blog/klingel/klingel1.jpg
tags: ["hackerspace", "SIP", "2025", "repair" , "klingel"]
---

## Alarm ist zurück
Unsere SIP-Türklingel, welche seit einigen Monaten im Einsatz war, hat leider ihren Dienst unverhofft verweigert. Doch die z-Labor-Mitglieder flox und anno haben sie auseinandergenommen, am offenen Herzen operiert, lobotomiert und wieder zum Leben erweckt.

Was war überhaupt kaputt? Zwar konnte die Türklingel Datenpakete aus dem Netzwerk empfangen, selbst jedoch keine schicken. Es lag aber nicht daran, dass die Türklingel schüchtern war sondern es bestand Verdacht auf einen Hardwarefehler, eventuell ein BGA-Ball der keinen Kontakt mehr hat. Also haben wir das Gerät aufgeschraubt und geschaut, was hinter der Fassade steckt. 

Es gibt viele Anschlüsse auf der Platine und interessanterweise ein seriales Interface und eine unbestückte USB-Schnittstelle. Während des Boot-Vorgangs des Gerätes wurde auf der seriellen Konsole angezeigt, dass es sich um ein minimales Betriebssystem auf Linux-und Busyboxbasis handelt. Nach ein wenig ausprobieren hat flox eine Root-Shell bekommen – ohne Logindaten eingeben zu müssen. Über die USB-Schnittstelle wurde ein USB-LAN Adapter angelötet und da die USB-Treiber im Kernel schon enthalten und beim Booten geladen waren, wurde der Adapter gleich erkannt. Durch die root-Shell konnten wir das USB-Ethernet-Interface umbenennen damit es so heisst wie das dysfunktionale interne, und somit läuft die proprietäre eingebaute Software, als ob nie etwas gewesen wäre. Es muss allerdings auch Power-Cycle fest sein, denn man kann ja nicht nach jeden Stromausfall mit den Laptop zur Klingel laufen. Also was nun? Einfach ein Digispark Attiny85 an die serielle Schnittstelle anlöten, sodass der Digispark die Kommandos automatisch nach ~45s nach Power on eintippt:

```bash
killall netconfig                   # Originalen DHCP-Client beenden, wird nach ~10s von eingebauten Init-System neu gestartet
ip link set dev eth0 down           # Defektes Interface abschalten
ip link set dev eth0 name kaputt0   # Defektes Interface umbenennen um es "aus den Weg zu schaffen"
ip link set dev eth1 name eth0      # Das USB-Interface so benennen wie das Eingebaute Interface
ip link set dev eth0 up             # Und das USB-Interface wieder aktivieren (Tadaaa, heisst jetzt so wie das defekte)
```


![Leiterplatte, Kabel, chaotischer Tisch](/blog/klingel/klingel4.jpg)

Was wäre ein Projekt ohne Heißkleber? Damit es wirklich fertig ist, muss alles mit Heißkleber befestigt werden. Die gehackte/reparierte Türklingel ist nun wieder erfolgreich im Einsatz, also klingelt uns gern mal an.

![Leiterplatte der Türklingel in Nahaufnahme mit darauf liegenden in Isolierband eingewickelten USB-Netzwerkadapater](/blog/klingel/klingel2.jpg)
![Türklingel und z-Labor Logo an Außenfassade der Kulturweberei](/blog/klingel/klingel3.jpg)

Aktuelles findet ihr zudem auf Chaos.Social unter: [chaos.social/@zLabor](https://chaos.social/@zLabor)

(z-Redaktion)
---
title: 'Erste Projekte'
date: 2015-01-18T22:10:00+06:00
featureImage: /blog/erste-projekte/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "2015"]
---

**LICHTundLAUNE**

Moderne Lichtgestaltung; Mit Mikrocontroller und LED’s lässt sich überall ein Highlight setzen. Wir versuchen uns an verschieden Steuerungs- und Lichtkonzepten.

![](/blog/erste-projekte/2014-05-22-16-48-32.webp)
![](/blog/erste-projekte/img_5058.webp)

**RasPi-Standalone**

Zur Erweiterung unseres Hardware-Repertoires bauen wir eine RaspberryPi B mit integriertem 3.2″ Touchscreen zusammen.

![](/blog/erste-projekte/2015-01-18-18-24-31.webp)

Das StandaloneSystem kann dann für viele Anwendungen sinnvoll eingesetzt werden. Vorrangige Hauptanwendung wird wohl die gezielte Musikversorgung sein. Weiterhin ist das RasPi mit Display auch für viele Mess- und Steueraufgaben geeignet.

![](/blog/erste-projekte/2015-01-18-17-03-18.webp)
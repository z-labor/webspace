---
title: 'Der erste LPD im z-Labor'
date: 2019-05-25T00:29:00+06:00
featureImage: /blog/erster-lpd-im-z-labor/thumbnail.webp
tags: ["hackerspace", "makerspace", "lpd", "zwickaugehtauchanders", "linux", "zwickau", "2019", "opensource"]
---

Linux interessiert nicht nur Jung sondern auch Alt: Noch während der Vorbereitungen stand der erste Gast in der Tür. Und damit auch die erste Überraschung. Ein Herr älteren Jahrgangs mit den Worten: „Hallo, ich bin der totale Computer-Profi!“. Er wurde von uns mit offenen Augen und Armen empfangen. Und weil er natürlich sein Notebook unter dem Arm hatte konnte es gleich losgehen.

![](/blog/erster-lpd-im-z-labor/img_8129.webp)

Es dauerte nicht lang und es stand der nächste Gast in der Tür. Er interessierte sich neben Linux als Betriebssystem auch für die Hardware seines Notebooks. Genauer gesagt für Speichererweiterung.

Und so wurde es schnell gemütlich im z-Labor. Wir zählten 15 Gäste von denen knapp die Hälfte ihre eigene Hardware mitbrachte. Vier mal wurden Linux-Distributionen installiert, meist neben dem alten Windows. Aber auch einen endgültigen Wechsel von XP zu Linux gab es. Das Hauptinteresse galt Distros mit einer Windows 7 ähnlichen grafischen Oberfläche und einfachen Bedienung. So war es gut, dass genug verschiedene USB-Sticks zum Test vorhanden waren. Auch mitgebrachte USB-Sticks wurden mit Linux Livesystemen bespielt und mit nach Hause genommen.

![](/blog/erster-lpd-im-z-labor/img_8130.webp)
![](/blog/erster-lpd-im-z-labor/img_8128.webp)
![](/blog/erster-lpd-im-z-labor/img_8116.webp)
![](/blog/erster-lpd-im-z-labor/img_8124.webp)
![](/blog/erster-lpd-im-z-labor/img_8122-e1558868099885.webp)
![](/blog/erster-lpd-im-z-labor/img_8120-1-e1558821266175.webp)

Alles in allem eine gelungene Veranstaltung. So konnte der erste Linux Presentation Day zu Ende gehen. Der nächste kommt bestimmt.
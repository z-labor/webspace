---
title: 'Der FahrStuhl: ein Fahrradsattel aus Holz #1'
date: 2017-07-19T22:11:00+06:00
featureImage: /blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/thumbnail.webp
tags: ["hackerspace", "makerspace", "2017", "projekte"]
---

Unser Mitglied [Christopher](https://twitter.com/ch_taudt) hat sein Projekt **Holzsattel** zu einem vorläufigen Ende gebracht. Der Sattel, der jetzt den Namen der **FahrStuhl** trägt, ist fertig gebaut, trägt seinen Besitzer und ist fahrbar. So sieht er aus:

![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20170708-dsc_03121.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20170708-dsc_0089.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20170708-dsc_01001.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20170708-dsc_01231.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20170708-dsc_01391.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20170708-dsc_01531.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20170708-dsc_02311.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20170708-dsc_02501.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20170708-dsc_02641.webp)
![](/blog/der-fahrstuhl-ein-fahrradsattel-aus-holz-1/20170708-dsc_03071.webp)


In Teil 2 und 3 stellen wir euch die technischen Details des Sattels vor.
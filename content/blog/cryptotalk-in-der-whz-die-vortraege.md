---
title: 'CryptoTalk in der WHZ: Die Vorträge'
date: 2019-10-21T01:07:00+06:00
featureImage: /blog/placeholder.webp
tags: ["hackerspace", "makerspace", "2019", "whz", "verschlüsselung", "datenschutz", "zwickaugehtauchanders"]
---

Für alle, die am 07.11.2019 zu unserem Cryptotalk waren oder die einfach interessiert sind, haben wir die Vorträge zu den Themen zum Download bereitgestellt:

[E-Mail-Verschlüsslung.pdf](/blog/cryptotalk-in-der-whz-die-vortraege/E-Mail-Verschl%C3%BCsslung.pdf)

[Speicherverschlüsslung.pdf](blog/cryptotalk-in-der-whz-die-vortraege/Speicherverschl%C3%BCsslung.pdf)

[Sicher-im-Web.pdf](blog/cryptotalk-in-der-whz-die-vortraege/Sicher-im-Web.pdf)

Falls ihr Fragen habt, könnt ihr auch gern immer Mittwochs ab 19:30 Uhr bei uns im Hackerspace z-Labor in der Seilerstraße 1 vorbeikommen.
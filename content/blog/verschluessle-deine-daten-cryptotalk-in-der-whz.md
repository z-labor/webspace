---
title: 'Cryptotalk in der WHZ: Verschlüssle deine Daten!'
date: 2019-10-18T00:46:00+06:00
featureImage: /blog/verschluessle-deine-daten-cryptotalk-in-der-whz/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "2019", "veranstaltung", "opensource", "vortrag", "whz", "crypto", "datenschutz"]
---

Das z-Labor bietet am **07. November 2019** an der Westsächsischen Hochschule Zwickau im Rahmen des Studium Generale einen Infovortrag zu den Grundzügen der Verschlüsselung an und stellt wichtige Tools zur digitalen Selbstverteidigung vor. **Ab 17:30 Uhr** werden wir im **Raum GAB202** in erster Linie auf Themen wie E-Mailverschlüsselung, Sicherheitserweiterungen für Webbrowser, die Generierung und sichere Verwahrung von Passwörtern sowie die Verschlüsselung von Festplatten eingehen.

Zeit: 07.11.2019, 17:30 – 20:30 Uhr
Ort: WHZ, Campus Innenstadt GAB202
Weitere Informationen findet ihr [hier](https://www.fh-zwickau.de/studierende/uebergreifende-lehrangebote/studium-generale/einzelveranstaltungen/).

Dieser Vortrag richtet sich zunächst **ausschließlich an Studierende der WHZ**. Wir sind allerdings gern dazu bereit den Vortrag auch im Rahmen anderer Veranstaltungen anzubieten. Falls ihr Interesse daran habt, könnt ihr uns unter [info@z-labor.space](info@z-labor.space) kontaktieren.
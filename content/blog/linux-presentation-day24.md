---
title: 'Linux Presentation Day'
date: 2024-10-29T00:52:00+06:00
featureImage: /blog/lpd24/lpd2024.png
tags: ["hackerspace", "zwickau", "2024", "linux", "linuxpresentationday", "lpd", "veranstaltung", "opensource"]
---
## Freie Software in Aktion

Anlässlich des [Linux Presentation Day](https://l-p-d.org/:en:start) (LPD) präsentieren wir euch am Samstag, den 16. November 2024, wie leicht es ist auf Linux umzusteigen und beantworten all eure Fragen rund um das freie Betriebssystem und Open-Source-Software bzw. Freie Software generell.

Wir haben Erfahrungen bei der Nutzung von freien Programmen wie Kicad, Cura, Firefox, Thunderbird, Inkskape, Dark Table, Audacity, KDElive, KeePassXC, Matrix, Mumble uvm. Wir teilen unser Wissen gern.
Ihr könnt einen Computer mitbringen oder einfach so vorbeikommen und erfahren, wie die Installation funktioniert, welche Programme euch für den Alltag oder ganz spezielle Aufgaben zur Verfügung stehen und was sich neben einem Desktopcomputer/Laptop noch mit Linux betreiben lässt.

Besucht uns am 16.11. zwischen 13:00 und 19:00 Uhr im z-Labor in der Kulturweberei, Seilerstraße 1, 08056 Zwickau! USB-Sticks mit verschiedenen Linux-Systemen liegen schon zum Testen für euch bereit. 🙂

Aktuelles findet ihr zudem auf Chaos.Social unter: [chaos.social/@zLabor](https://chaos.social/@zLabor)
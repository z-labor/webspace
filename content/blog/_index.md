---
title: "z-Labor Blog"
---

Hier bekommst du Aktuelles und Unwichtiges aus dem Zwickauer Hackerspace. Andere wichtige Kanäle sind das Wiki (to be built) und vor allem der IRC Kanal [#z-Labor](irc://irc.hackint.eu/#z-labor) auf Hackint. Mails lesen wir auf [info@z-Labor.space](mailto:info@z-labor.space). Hauptsächlich aber: Donnerstags ist immer ab 19:00 Uhr offener Chaostreff in unserem Space, Seilerstraße 1, 08056 Zwickau!
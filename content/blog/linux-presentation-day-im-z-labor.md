---
title: 'Linux Presentation Day'
date: 2019-04-16T00:24:00+06:00
featureImage: /blog/linux-presentation-day-im-z-labor/thumbnail.webp
tags: ["hackerspace", "makerspace", "lpd", "linux", "opensource", "2019"]
---

In diesem Jahr nehmen wir am [Linux Presentation Day](https://l-p-d.org/) teil. Am 18.05.2019 präsentieren wir euch alles was man mit Linux machen kann, zeigen euch wie leicht man auf das freie Betriebssystem umsteigen kann und beantworten all eure Fragen rund um Linux, freie Software und Open Source. Der LPD ist eine Art Tag der offenen Tür, der weltweit stattfindet. Orte, die sich mit den Themen Computer und Freie Software beschäftigen, also zum Beispiel Schulen, Hackerspaces oder Linux User Groups, öffnen ihre Türen um allen Interessierten das Thema Linux näher zu bringen. Seit 2015 geht das schon so und dieses Jahr sind wir das erste Mal dabei. Ihr könnt gern einen Computer mitbringen oder einfach so vorbeikommen und euch zeigen lassen wie einfach eine Installation ist, welche Programme euch für den Alltag oder ganz spezielle Aufgaben zur Verfügung stehen und was man außer einem normalen Desktopcomputer/Laptop noch mit Linux betreiben kann.

Wir sehen uns am **18.05.2019 von 11-19:00 Uhr** im z-Labor, Seilerstraße 1, 08056 Zwickau.

![](/blog/linux-presentation-day-im-z-labor/linux-presentation-day-im-z-labor.webp)
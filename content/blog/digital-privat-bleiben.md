---
title: 'Digital privat bleiben'
date: 2025-03-01T00:52:00+06:00
featureImage: /blog/digital-privat/Erdman.png
tags: ["studiumgenerale", "workshop", "2025", "WHZ" , "meko#mobil"]
---

## Workshop am 19.03.25
Datenskandale, Geheimdienstaffären, Internetkriminalität und Cybermobbing. Die letzten Jahre haben gezeigt, wie fragil die eigene Privatsphäre im Internet ist und wie wichtig digitale Emanzipation und Selbstverteidigung sein können.

Dieser Workshop gibt daher einen Überblick über grundlegende Verschlüsselungstechniken und zeigt praktisch, wie sich E-Mails und Daten verschlüsseln lassen, wie sichere Passwörter erstellt und verwaltet werden und wie Tracking im Netz verhindert werden kann. Auch der Datenschutz auf Mobilgeräten wird eine Rolle spielen. Es werden freie Tools vorgestellt, die einen verantwortungsvollen Umgang mit persönlichen und fremden Daten ermöglichen und digitale Kommunikation besser absichern. 

Wie kommuniziere ich sicher im digitalen Raum und schütze meine Privatsphäre im Netz? Wie funktioniert Kryptografie und welche freien Tools und Sicherheitseinstellungen haben sich bewährt? Antworten gibt es bei dieser kostenfreien Kooperationsveranstaltung unseres Vereins mit dem Projekts [MeKo#mobil](https://www.mekomobil.de/) und der [Westsächsischen Hochschule Zwickau](https://www.fh-zwickau.de/studium/studierende/uebergreifende-lehrangebote/studium-generale/einzelveranstaltungen/) - nicht nur für Studierende, sondern offen für alle.

Das Mitbringen des eigenen Laptops ist erwünscht. 

### Zeiten und Details:
- **Datum:** 19.03.25
- **Uhrzeit:** 16 Uhr - 20 Uhr
- **Ort:** 

        Westsächsische Hochschule Zwickau
        Campus Innenstadt
        Georgius-Agricola-Bau
        Raum GAB304

        Dr.-Friedrichs-Ring 2A
        08056 Zwickau
---

![Logo WHZ](/blog/digital-privat/WHZ.svg)
![Logo Meko#mobil](/blog/digital-privat/mekomobil.png)
![Logo Mekosax](/blog/digital-privat/mekosax.png)
![Logo SLM](/blog/digital-privat/SLM.png)

Aktuelles findet ihr zudem auf Chaos.Social unter: [chaos.social/@zLabor](https://chaos.social/@zLabor)

(z-Redaktion)
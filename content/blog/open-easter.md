---
title: 'Open Easterspace'
date: 2024-03-25T13:37:00+06:00
featureImage: /blog/open-easter/openspace.jpg
tags: ["hackerspace","hackspace", "zwickau", "2024", "kulturweberei", "easterhegg", "mastodon" , "livecoding"]
---

## Hallo liebe Leute,

am internationalen Tag des offenen Hackspaces (**Samstag, den 30.03.24**) laden wir euch **ab 13:37 Uhr** ins z-Labor nach Zwickau ein. Es wird Waffeln, Tschunk, [Easterhegg](https://eh21.easterhegg.eu/)-Streams sowie Vorträge zum Thema Stable Diffusion (KI-Bildgenerator) und Krach&Gravitation mit [harte echtzeit](https://sonomu.club/@harte_echtzeit) geben.

### Zeiten und Details:
- **Datum:** 30.03.24
- **Uhrzeit:** 13:37 Uhr - 23:42 Uhr

----
* **13:37:** [Easterhegg](https://fahrplan.eh21.easterhegg.eu/eh/schedule/#2024-03-30) im Livestream
* **15:00:** How to Stable Diffusion & Prompt Battle
* **17:30:** Waffeln
* **20:22:** How to Livecoding mit [tidalcycles](https://tidalcycles.org/)
* **21:00:** Krach&Gravitation mit [harte echtzeit](https://sonomu.club/@harte_echtzeit)

---

### Das Waffeleisen steht bereit:

![](/blog/open-easter/waffeln.jpg)

    Ort:
        z-Labor e.V.
        Kulturweberei
        Seilerstraße 1
        08056 Zwickau
        Haus C, Box 39
    Kontakt:
        Mail - info@z-labor.space
        Matrix - https://matrix.to/#/#public:z-labor.space

---

Aktuelles findet ihr im Fediverse unter: <a href="https://chaos.social/@zLabor" rel="me">[chaos.social/@zLabor]</a>
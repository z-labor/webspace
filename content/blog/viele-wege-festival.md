---
title: 'Viele Wege Festival 2024'
date: 2024-04-07T13:37:00+06:00
featureImage: /blog/viele-wege-festival/vielewege.jpg
tags: ["hackerspace","hackspace", "zwickau", "2024", "festival", "straßenfest", "workshop", "kostenlos", "zwickaugehtauchanders"]
---

## „Es gibt viele Wege Zwickau noch schöner zu machen“

Wir beteiligen uns zusammen mit vielen weiteren Akteuren aus der Zwickauer Kunst- und Kulturszene am Viele Wege Festival. Im Projekt 46 werden wir im Zuge des Mitmach-Straßenfests am **Sonntag (28.04.) ab 14 Uhr** Lötworkshops mit hübschen Bausätzen gegen Spende anbieten und unseren 3D-Drucker schmucke Accessoires zum Mitnehmen produzieren lassen. Zudem könnt ihr gegen uns 1D-Pong spielen. 

Wir werden jede Menge Katzenohrenringe mit Farbwechselfilament drucken. Kommt vorbei und lasst euch einen anfertigen! :)

![](/blog/viele-wege-festival/katzenohrenringe.jpg)

Das sehr vielfältige, erlebnisreiche [Festival-Programm](https://vielewege-fuerzwickau.de/programmuebersicht), wo für alle was dabei ist, findet vom 25.04. bis 01.05.24 statt. 

Weitere Infos unter [vielewege-fuerzwickau.de](https://vielewege-fuerzwickau.de)

---
    Zeit: 
        28.04.24 (Sonntag)
        14:00 - 18:00
    Ort:   
        Projekt 46
        Hauptstraße 46
        08056 Zwickau    

    Kontakt:
        Mail - info@z-labor.space
        Matrix - https://matrix.to/#/#public:z-labor.space
---

Aktuelles findet ihr zudem im Fediverse unter: [chaos.social/@zLabor](https://chaos.social/@zLabor)

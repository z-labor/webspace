---
title: 'Innovation Night im z-Labor'
date: 2017-11-07T22:01:00+06:00
featureImage: /blog/innovation-night-im-z-labor/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "2017", "veranstaltung", "InnovationNight"]
---

An der Westsächsische Hochschule Zwickau ist am 25.10. das [SAXEED Innovation Camp](https://www.fh-zwickau.de/studenten/berufseinstieg-unternehmensgruendung/gruendernetzwerk-saxeed/ueber-saxeed/) gestartet. Bei der Kick-off-Veranstaltung im Rahmen von „Die Nacht der großen Probleme“ haben Studierende unterschiedlicher Fachrichtungen, Professoren und Mitarbeiter in Kooperation mit dem Startup Stadt.Land.Netz aus Dresden Problemstellungen erarbeitet. Diese werden Grundlage für die folgenden Veranstaltungen sein, in denen dann Lösungen bzw. Prototypen entwickelt und Crowdfunding-Kampagnen gestartet werden. 

**Die nächste Veranstaltung – Innovation Nights -Session I – findet am 15.11. 2017 ab 18 Uhr bei uns im z-Labor statt.**

In Teams sollen bei Club Mate und chilliger Musik an diesem Abend erste Lösungskonzepte zu den erarbeiteten Problemstellungen erstellt werden.
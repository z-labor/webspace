---
title: 'Eindrücke vom Schlachthof'
date: 2015-07-06T17:56:00+06:00
featureImage: /blog/eindruecke-vom-schlachthof/thumbnail.webp
tags: ["hackerspace", "makerspace", "2015"]
---

In Kürze müssen wir unsere Location in der Zwickauer Schlachthofstraße verlassen. Momentan suchen wir noch nach passenden neuen Räumen. Habt ihr Vorschläge für einen Raum zwischen 30m² und 50m², der auch als Werkstatt geeignet ist? Dann immer her damit!

Zum Abschluss gibt es noch ein paar Bilder des alten Spaces:

![](/blog/eindruecke-vom-schlachthof/dsc_0324.webp)
![](/blog/eindruecke-vom-schlachthof/dsc_0326.webp)
![](/blog/eindruecke-vom-schlachthof/dsc_0328.webp)
![](/blog/eindruecke-vom-schlachthof/dsc_0331.webp)
![](/blog/eindruecke-vom-schlachthof/dsc_0340.webp)
![](/blog/eindruecke-vom-schlachthof/dsc_0342.webp)
![](/blog/eindruecke-vom-schlachthof/dsc_0354.webp)
---
title: 'Lesung mit Berit Glanz'
date: 2020-03-03T01:17:00+06:00
featureImage: /blog/lesung-mit-berit-glanz/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "2020", "veranstaltung", "lesung", "kultur", "literatur"]
---

Am 11.03. 2020 haben wir die Autorin [Berit Glanz](https://www.beritglanz.de/) zu Gast. Sie wird aus ihrem Debütroman „[Pixeltänzer](https://www.schoeffling.de/buecher/berit-glanz/pixelt%C3%A4nzer)“ lesen. Der Roman schafft es die Brücke zwischen Analogem und Digitalem, zwischen Gegenwart und Vergangenheit zu bauen. Die Protagonistin Beta arbeitet nicht nur in einem Startup und erstellt in ihrer Freizeit 3D-gedruckte Insekten, sie lässt sich auch per Online-Schnitzeljagd in die Geschichte eines Künstlerpaares aus den 1920er Jahren verwickeln. Und mit ihr taucht natürlich auch der Leser in beide Geschichten ein…

Ab 19 Uhr öffnen sich für euch die Tore des Veranstaltungsorts, der [Kevin Brewery](https://www.facebook.com/events/503741273658947/) bei uns in der Seilerstraße 1. Ab 19:30 Uhr wird gelesen. Da der Eintritt frei ist, würden wir uns über die ein oder andere Spende freuen. Kommet in Scharen!!!

Gefördert wird die Lesung vom Kulturamt der Stadt Zwickau, vom Kulturraum Vogtland-Zwickau sowie vom Landkreis Zwickau. Unterstützung bekommen wir durch die [Kevin Brewery](http://www.kevin-brewery.de/) und die [Buchhandlung Marx](https://www.buechermarx.com/).

![](/blog/lesung-mit-berit-glanz/lesung_bg_v2.gif)

Bildnachweis: Autorinfoto = Berit Glanz privat | Tobogganmaske = public domain in [Sammlung](https://sammlungonline.mkg-hamburg.de/de/object/Tanzmaske-%22Toboggan-Frau%22-von-Lavinia-Schulz/P1994.44.13/mkg-e00132702)

![](/blog/lesung-mit-berit-glanz/lesungsplakat_foerdergeber.webp)
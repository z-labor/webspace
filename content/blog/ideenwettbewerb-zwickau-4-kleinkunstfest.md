---
title: 'Ideenwettbewerb Zwickau #4: Kleinkunstfest'
date: 2016-04-12T15:56:00+06:00
featureImage: /blog/ideenwettbewerb-zwickau-4-kleinkunstfest/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "ZwickauGehtAuchAnders", "2016", "ideenwettbewerb"]
---

Folge Nummer vier unserer Serie zum [Zwickauer Ideenwettbewerb](http://www.zwickau.de/de/politik/aktuelles/ideenwettbewerb.php?shortcut=ideenwettbewerb). Ihr habt Spaß am Leben in der Stadt? Fressbuden sind nicht das Einzige was das Stadtfest auszeichnet? Dann ist diese Idee wahrscheinlich etwas für euch. Wir wünschen uns das Mehr an Unterhaltung, Kultur, Klamauk und letztlich Spaß. Nicht nur weihnachtsmarkteskes Rumgeschubse sondern ein lockeres Kleinkunstfest:

##### ###### Straßen- bzw. Kleinkunstfeste
> **Ziel:**
> 
> Belebung der Stadt durch schaffung von attraktiven Freizeitangeboten am Wochenende!
> 
>_ „Es muss so viel los sein, das die Studenten gar nicht nachhause fahren möchten!“_
> 
> Kleines Fest im Großen Garten, in Hannover ist eine Veranstaltung von bekannten und unbekannten Schaustellern in einen abgesperten Bereich.
> 
> Mehr Straßenfeste bei denen sich die lokalen Geschäfte beteiligen.
> 
> **Veranstaltungsorte:**
> 
> Straßenfeste(Innenstadt, Marktplatz und umliegende Straßen)
> 
> Kleinkunstfest(Schwanenteich)
> 
> **Vorbilder:**
> 
> Kleines Fest im Großen Garten aus Hannover [kleines-fest.de](http://kleines-fest.de/)
> 
> Limmerstraßenfest Hannover

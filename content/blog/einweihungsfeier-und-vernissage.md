---
title: 'Einweihungsfeier und Vernissage'
date: 2023-09-20T13:37:00+06:00
featureImage: /blog/einweihungsfeier-und-vernissage/opening.jpg
tags: ["hackerspace", "makerspace", "zwickau", "2023", "kulturweberei", "mastodon"]
---

Wir haben uns räumlich vergrößert und weihen am **Samstag, den 23.09.23 ab 16:00 Uhr** unseren neuen Raum in der Kulturweberei Zwickau ein. Wir möchten feiern, was wir besonders in den letzten Monaten geschafft und im Verein sowohl analog als auch digital voran gebracht haben.

Zusätzlich gibt es eine Vernissage von [Mondstern](https://moooon.dresden.network/) mit Bildern von freier Software sowie Kunstwerke von [gusano estupido](
https://instagram.com/gusano_estupido) aus Zwickau zu sehen.
Es wird die ein oder andere Sache zu entdecken geben. Computer, Lötbausätze, Stickerboxen und vieles mehr stehen zum Ausprobieren und Erkunden bereit.

Wir freuen uns auf alle, die den Weg zu uns finden :) :) :)

_Eure Chaoswesen vom z-Labor Zwickau_

---

Veranstaltungsdetails

```
    Datum:
        23.09.23
    Uhrzeit:
        ab 16 Uhr
    Ort:
        z-Labor e.V.
        Kulturweberei
        Seilerstraße 1
        08056 Zwickau
        Haus C, Box 39
    Kontakt:
        Mail - info@z-labor.space
        Matrix - https://matrix.to/#/#public:z-labor.space
```

Aktuelles findet ihr im Fediverse unter: [chaos.social/@zLabor](https://chaos.social/@zLabor)

---

Mehr über unsere Künstler erfahrt ihr hier

- Mondstern: https://moooon.dresden.network/ --- https://forum.f-droid.org/t/5-f-droid-ausstellung/23570
- gusano estupido: https://instagram.com/gusano_estupido
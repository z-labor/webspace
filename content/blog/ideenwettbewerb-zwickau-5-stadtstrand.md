---
title: 'Ideenwettbewerb Zwickau #5: Stadtstrand'
date: 2016-04-16T21:05:00+06:00
featureImage: /blog/ideenwettbewerb-zwickau-5-stadtstrand/thumbnail.webp
tags: ["hackerspace", "makerspace", "zwickau", "ZwickauGehtAuchAnders", "2016", "ideenwettbewerb"]
---

Und hier sind wir schon beim letztenm Teil unserer Serie an [Ideen für die Aufwertung der Zwickauer Innenstadt](http://www.zwickau.de/de/politik/aktuelles/ideenwettbewerb.php?shortcut=ideenwettbewerb).  Nachdem wir in Idee Nummer 4 gefeiert haben wird es nun Zeit etwas zu entspannen. Kommt mit uns in die Utopie eines Strandes in Innenstadtnähe mit Beachbar, Chill-Out-Lounge, Grillplätzen und allem was ihr euch sowieso schon immer gewünscht habt ;-)

##### ###### Stadtstrand/Grillwiese an der Mulde

> **Ziel:**
> 
>  Belebung der Grünflächen an der Mulde. Durch Erschaffung von Freizeitmöglichkeiten und erhöhung des Wohlfühlfaktors an der Innenstadt.
>  
>  Ansiedlung einer Beach Bar
>  
>  Errichtung für Freigegebene Flächen zum Grillen und Beachvolleyball spielen.
> 
> **Veranstaltungsorte:**
> 
> Grünflächen an der Mulde
> 
> **Vorbilder:**
> 
> Bitterfeld Baari Beach
> 
> Leipzig La Playa Beach-Club
> 
> Greppin Beachvolleyball
> 
> Dortmund Grillwiesen
> 
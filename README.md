# ![z-Labor Logo](static/images/zlab_logo.svg) z-Labor Webspace

## Was ist denn das hier?

Dies ist der Quellcode zur Webseite des z-Labors. Der Code liegt hier auf [Codeberg](https://codeberg.org/), die Webseite wird über eine Pipeline gebaut und auf den Webserver übertragen.

Am Ende landet alles hier: **https://www.z-labor.space**

## Technisches Blabla zum Verständnis

Die Webseite wird mit dem Static Site Generator [Hugo](https://gohugo.io) gebaut. Dies ist ein [CLI](https://en.wikipedia.org/wiki/Command-line_interface) Programm, welches auf Grundlage der Einstellungen aus der [`config.toml`](config.toml) die [html](https://www.w3schools.com/html/)-, [scss](https://sass-lang.com/documentation/syntax)-, [js](https://www.w3schools.com/js/default.asp)-Dateien und statischen Assets (Bilder, [Icons](https://github.com/topics/icons), [Fonts](https://github.com/topics/font) etc.) zusammen mit den [Markdown](https://daringfireball.net/projects/markdown/)-Textdateien zusammenbaut und eine [statische Webseite](https://en.wikipedia.org/wiki/Static_web_page) generiert.

Da der Code mittels [git](https://git-scm.com/) versioniert wird und wir [Codeberg](https://codeberg.org) als Git-Hoster verwenden, können wir auf Codebergs CI/CD-Fähigkeiten setzen und eine sogenannte Pipeline den [Buildprozess](https://en.wikipedia.org/wiki/Software_build) automatisiert nach jedem [Commit](https://git-scm.com/docs/git-commit) ausführen lassen. Der Ablauf ist aktuell noch nicht vollständig implementiert - wird aber nachgereicht.

## Die Webseite lokal zum Laufen kriegen

Ein [Deployment](https://en.wikipedia.org/wiki/Software_deployment) auf [localhost](https://en.wikipedia.org/wiki/Localhost) funktioniert genauso, wie auch der Buildprozess die Webseite erstellt. Man kann also die Webseite zum Testen und beim Vornehmen von Änderungen erst lokal anschauen und dann später erst online [pushen](https://git-scm.com/docs/git-push).

### Hugo

Zunächst benötigt man Hugo. Das Tool ist zwar [cross-plattform](https://en.wikipedia.org/wiki/Cross-platform_software), es gibt aber wie immer je nach System einige Besonderheiten. Deshalb, sollte man stets die [offizielle Installationsanleitung](https://gohugo.io/getting-started/installing/) auf der Hugo Webseite für das jeweilige System verwenden.

Wenn es richtig installiert wurde und im Pfad verfügbar ist, solltest du mit folgendem Kommando im [Terminal](https://learning.lpi.org/en/learning-materials/010-160/2/2.1/2.1_01/) die Hilfe sehen können:

`$ hugo -h`

Linux Systeme:

    Debian
    - Hat leider ein uraltes Package, welches nicht maintained wird

    Ubuntu
    - Nutze am besten snap (siehe Debian)

    Linux Mint
    - Nutze am besten snap (siehe Debian)

    Arch
    - offizielle Paketquellen (pacman -S hugo) 

    Gentoo
    - Du baust es dir doch sowieso selbst - hier geht's zum Code: https://github.com/gohugoio/hugo


Andere Systeme:
    
    Windows
    - Ist doof (geht aber trotzdem)
    - Achte darauf, dass hugo-extended installiert ist (die normale Version hat Probleme bei der Neugenerierung vom SCSS)

    MacOS
    - Zur Installation braucht es homebrew oder man lädt es als binary

Die aktuell für die Webseite verwendete Hugo Version ist: `v0.122.0`

Du kannst herausbekommen, welche Version bei dir installiert ist, wenn du folgendes in ein Terminal eingibst:

`$ hugo version`

### Für busy Leute

1. Auf https://z-labor.space gehen
2. Fehler oder Möglichkeiten zur Verbesserung finden
3. Screenshots erstellen oder eine Skizze mit Krita malen
4. Auf Codeberg zum Issuetracker gehen
5. Issue erstellen

### Für Anfänger: Manuelles Vorgehen

1. Lade dir das [Repository](https://codeberg.org/z-labor/webspace) herunter: [Downloadlink](https://codeberg.org/z-labor/webspace/archive/main.zip)
2. Öffne ein Terminal im Downloadverzeichnis (oftmals klappt das mittels Rechtsklick -> Terminal hier öffnen)
3. Wechsle in das Hauptverzeichnis `webspace`
   - `$ cd webspace`
4. Prüfe, ob du im richtigen Pfad bist
   - `$ ls`
   - Du solltest nun in der Ausgabe die Datei `config.toml` finden
5. Starte den lokalen Buildprozess
   - `$ hugo serve`
   - Die Webseite wird gebaut und es taucht folgende Meldung auf:
   - `Web Server is available at http://localhost:1313/ (bind address 127.0.0.1) Press Ctrl+C to stop`
6. Öffnen deinen Lieblingsbrowser und navigiere zu folgender Adresse:
    - `localhost:1313`
7. Nun solltest du die Webseite sehen können (🥳 *JUHUU, geschafft!* 🎉)
8. Öffne jetzt eine beliebige Datei, mach eine Anpassung und sieh, wie sich die Webseite auf magische Weise aktualisiert und deine Anpassung sofort sichtbar wird
9. Erstelle in der [Issue Liste](https://codeberg.org/z-labor/webspace/issues) ein neues Issue mit einem Klick auf [New Issue](https://codeberg.org/z-labor/webspace/issues/new)
10. Füge deine Änderungsvorschläge ein und am besten einen Screenshot, was diese bewirken
11. Die Projektadmins kümmern sich um den Rest 😉

### Für busy Leute, die keine Vorschau benötigen

1. Navigiere im Codeberg Repository zum Pfad mit der Datei, die du anpassen möchtest
2. Klicke auf die Datei, sodass du zur Dateiansicht kommst - bspw.: https://codeberg.org/z-labor/webspace/src/branch/main/README.md
3. Klicke auf das Stift oben rechts im Menü mit dem Titel "Datei bearbeiten"
4. Mache deine Änderungen
5. Klicke auf den Tab "Vorschau der Änderungen" und gib deinem Commit einen Namen und eine Beschreibung
4. Klicke auf "Änderungen committen"
5. Fertig 😊

### Für Fortgeschrittene: Git Workflow

1. fork
2. `clone` (`fetch` -> `pull` aka sync)
3. `hugo server --disableFastRender` 
4. `commit`
5. `push`
6. merge request
7. merge

## Ordnerstruktur

Das Projekt hat eine für hugo-Webseiten typische Struktur. Es gibt Einstellungsdateien in der obersten Ebene und Unterordner mit bestimmten Funktionen.

    > content .......... Großteil der Webseitentexte
    > data ............. Einstellungsdateien für bestimmte Bereiche
    > resources ........ AUTOGENERIERT - Kann ignoriert werden
    > static ........... Bilder, Fonts und Plugins
    > themes ........... Theme-Ordner - Aktuell nur coHub
    .gitignore ......... Einstellungsdatei für git
    .hugo_build.lock ... AUTOGENERIERT - Kann ignoriert werden
    config.toml ........ Einstellungsdatei für Hugo
    LICENSE ............ Lizenztext
    README.md .......... Diese Readme :)